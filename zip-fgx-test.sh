#!/bin/sh
#< zip-fgx-test.sh - zip the source
BN=`basename $0`
TEMPV="0.0.1" # initial version 2012-02-10
PROJ="fgx-test"

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "Aborting ... no input!"
        else
            echo "Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE? ***"
}

if [ ! -f "../$PROJ/zip-fgx-test.sh" ]; then
    echo "ERROR: Not in correct folder... Can NOT find self..."
    exit 1
fi

echo "$BN: Will first do a full cleanup of ALL 'backups'..."
delallbu --nopause
echo "$BN: Done cleanup..."

TEMPZ="$PROJ-$TEMPV-src.zip"
TEMPS="$PROJ"
TEMPX="-x=$TEMPS/$TEMPS-build-desktop/* -x=$TEMPS/src/$TEMPS.pro.user"
TEMPO=""
if [ -f "../$TEMPZ" ]; then
    echo ""
    ls -all ../$TEMPZ
    echo "$BN: NOTE: *** This is an UPDATE ONLY *** File ../$TEMPZ ALREADY EXISTS!"
    echo ""
    TEMPO="-u"
fi
echo "$BN: Will run: zip $TEMPO -o -r $TEMPX $TEMPZ $TEMPS/*"
ask

cd ..
echo "$BN: In current directory [$(pwd)]"
if [ ! -d "$TEMPS" ]; then
    echo "ERROR: Can NOT locate $TEMPS folder!"
    exit 1
fi
echo "$BN: Doing: zip $TEMPO -o -r $TEMPX $TEMPZ $TEMPS/*"
zip $TEMPO -o -r $TEMPX $TEMPZ $TEMPS/*

if [ ! -f "$TEMPZ" ]; then
    echo "$BN: FAILED to create zip $TEMPZ"
    exit 1
fi

cd $TEMPS
ls -l "../$TEMPZ"

# eof
