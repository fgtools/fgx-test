@setlocal
@set TEMPV=0.0.1
@REM Begins at 2012-02-10
@set PROJ=fgx-test
@if NOT EXIST ..\%PROJ%\zip-fgx-test.bat goto ERR2

@set TEMPZ=%PROJ%-%TEMPV%-src.zip
@set TEMPS=%PROJ%
@set TEMPX=-x%TEMPS%\%TEMPS%-build-desktop\*.* -x%TEMPS%\src\%TEMPS%.pro.user
@set TEMPO=-a

@echo.
@if EXIST ..\%TEMPZ% (
@echo NOTE WELL: *** This is an UPDATE ONLY ***
@call dirmin ..\%TEMPZ%
@set TEMPO=-u
) else (
@echo NOTE WELL: *** This is a NEW FILE %TEMPZ% ***
)
@echo.

:GOTOPT

@echo RUN: zip8 %TEMPO% -o -P -r %TEMPX% %TEMPZ% %TEMPS%\*.*

@pause

@cd ..
@echo In current directory %CD%
@if NOT EXIST %TEMPS%\. goto ERR3

@echo Doing: call zip8 %TEMPO% -o -P -r %TEMPX% %TEMPZ% %TEMPS%\*.*
@call zip8 %TEMPO% -o -P -r %TEMPX% %TEMPZ% %TEMPS%\*.*

@if NOT EXIST %TEMPZ% goto ERR1

@call genzipindex %TEMPZ%
@call dirmin %TEMPZ%
@cd %TEMPS%

@goto END

:ERR1
@cd %TEMPS%
@echo ERROR: Zip NOT CREATED!
@goto END

:ERR2
@echo ERROR: Not in correct folder... Can NOT find self...
@goto END

:ERR3
@cd %TEMPS%
@echo ERROR: Can NOT locate %TEMPS% folder!
@goto END

:END
@endlocal
