#!/bin/sh
BN=`basename $0`
FILE="/tmp/fgx-test.txt"
if [ -f "$FILE" ]; then
    echo "$BN: output of [$FILE]..."
    ls -l $FILE
    cat $FILE
    echo "$BN: Done file [$FILE]"
else
    echo "$BN: File $FILE not located...";
    exit 1
fi
exit 0

# eof - typelog.sh
