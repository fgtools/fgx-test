/* ********************************************************
   *  loadAptDat
   *
   *  Created by Geoff R. Mclane, Paris
   *  (C) Dec 2011 GPL2 (or later)
   *
   *  Load and search flightgear apt.dat.gz
   *
   ******************************************************** */

#include "app_config.h"
#include "searchDialog.h"
#include "mainwindow.h"
#include "loadAptDat.h"

searchDialog::searchDialog(QWidget *parent) :
    QDialog(parent)
{
    int m = 10;
    int row = 0;
    int cols = 10;
    QString tmp;

    main = (MainWindow *)parent;

    mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(m,m,m,m);
    mainLayout->setSpacing(10);
    setLayout(mainLayout);

    info = new QLabel("Search by ICAO, and get closest airports. Green valid, red not found.",this);
    mainLayout->addWidget(info,row,0,1,cols);
    row++;

    icaoLab = new QLabel("ICAO:",this);
    icaoEd = new QLineEdit(this);
    cntLab = new QLabel("Count:",this);
    cntEd = new QLineEdit(this);

    mainLayout->addWidget(icaoLab,row,0,1,1);
    mainLayout->addWidget(icaoEd,row,1,1,1);
    mainLayout->addWidget(cntLab,row,2,1,1);
    mainLayout->addWidget(cntEd,row,3,1,1);
    row++;

    tmp = main->settings->value(S_ICAO,"").toString();
    icaoEd->setText(tmp);
    icao_list = main->settings->value(S_LISTICAO,"").toStringList();
    if (tmp.length() && !icao_list.contains(tmp))
        icao_list += tmp;
    on_icao_changed(tmp);

    cntEd->setText(main->settings->value(S_COUNT,"10").toString());

    connect(icaoEd,SIGNAL(textChanged(QString)),this,SLOT(on_icao_changed(QString)));

    // checkbox options
    xcludLab = new QLabel("Exclude:",this);
    xheliCheck = new QCheckBox("Heli",this);
    xseaCheck = new QCheckBox("Sea",this);
    xclosedCheck = new QCheckBox("Closed",this);

    xheliCheck->setChecked(main->settings->value(S_XHELI,"1").toBool());
    xseaCheck->setChecked(main->settings->value(S_XSEA,"1").toBool());
    xclosedCheck->setChecked(main->settings->value(S_XCLOSED,"1").toBool());

    mainLayout->addWidget(xcludLab,row,0,1,1);
    mainLayout->addWidget(xheliCheck,row,1,1,2);
    mainLayout->addWidget(xseaCheck,row,3,1,2);
    mainLayout->addWidget(xclosedCheck,row,5,1,2);
    row++;

    detailLab = new QLabel("Details:",this);
    runCheck = new QCheckBox("Runways",this);
    commCheck = new QCheckBox("Comms",this);
    runCheck->setChecked(main->settings->value(S_DETRUN,"0").toBool());
    commCheck->setChecked(main->settings->value(S_DETCOMM,"0").toBool());

    mainLayout->addWidget(detailLab,row,0,1,1);
    mainLayout->addWidget(runCheck,row,1,1,2);
    mainLayout->addWidget(commCheck,row,3,1,2);
    row++;

    infoText = new QPlainTextEdit(this);
    infoText->setReadOnly(true);
    QFont font("Courier New");
    font.setFixedPitch(true);
    infoText->setFont(font);
    mainLayout->addWidget(infoText,row,0,10,cols);
    row += 10;

    row++;
    srchBut = new QPushButton(this);
    srchBut->setText("Search");
    mainLayout->addWidget(srchBut,row,cols-2,1,1);
    connect(srchBut, SIGNAL(clicked()), this, SLOT(on_search()));

    okBut = new QPushButton(this);
    okBut->setText("Done");
    mainLayout->addWidget(okBut,row,cols-1,1,1);
    connect(okBut, SIGNAL(clicked()), this, SLOT(on_done()));

    if (main && main->airdata && main->airdata->getAirportCount()) {
        srchBut->setDisabled(false);
        QString msg;
        msg.sprintf("Apt.dat load: %d airports, %d runways",
                    main->airdata->getAirportCount(),
                    main->airdata->getRunwayCount() );
        infoText->setPlainText(msg);
    } else {
        srchBut->setDisabled(true);
        infoText->setPlainText("No airports loaded. Need to load Apt.dat first.");
    }
}

void searchDialog::on_done()
{
    // save settings
    if (main && main->settings) {
        QString icao(icaoEd->text());
        if (icao.length() && main && main->airdata && main->airdata->findAirportByICAO(icao)) {
            // save only a VALID findable ICAO
            main->settings->setValue(S_ICAO,icao);
        }
        main->settings->setValue(S_LISTICAO,icao_list);
        main->settings->setValue(S_COUNT,cntEd->text());
        main->settings->setValue(S_XHELI,(xheliCheck->isChecked() ? "1" : "0"));
        main->settings->setValue(S_XSEA,(xseaCheck->isChecked() ? "1" : "0"));
        main->settings->setValue(S_XCLOSED,(xclosedCheck->isChecked() ? "1" : "0"));
        main->settings->setValue(S_DETRUN,(runCheck->isChecked() ? "1" : "0"));
        main->settings->setValue(S_DETCOMM,(commCheck->isChecked() ? "1" : "0"));

    }
    close();
}

void searchDialog::on_search()
{
    QTime tm;
    QString msg, tmp;
    QString icao(icaoEd->text());
    int cnt = cntEd->text().toInt();
    tm.start();
    if (!main || !main->airdata)
        return;

    PAD_AIRPORT pap = main->airdata->findAirportByICAO(icao);
    if (pap) {
        bool xheli = xheliCheck->isChecked();
        bool xsea = xseaCheck->isChecked();
        bool xclosed = xclosedCheck->isChecked();
        int details = 0;
        if (runCheck->isChecked())
            details |= 1;
        if (commCheck->isChecked())
            details |= 2;

        msg = main->airdata->getAiportStg(pap,-1);
        tmp.sprintf("\nsearching for %d nearest...",cnt);
        msg.append(tmp);
        infoText->setPlainText(msg);
        AIRPORTLIST *pal = main->airdata->getNearestAiportList(pap);
        int i;
        int max = pal->count();
        int done = 0;
        for (i = 0; i < max; i++) {
            pap = pal->at(i);
            if (xheli && (pap->name.indexOf("[H]") >= 0))
                continue;
            if (xsea && (pap->name.indexOf("[S]") >= 0))
                continue;
            if (xclosed && (pap->name.indexOf("[X]") >= 0))
                continue;
            msg = main->airdata->getAiportStg(pap,details);
            infoText->appendPlainText(msg);
            done++;
            if (done >= cnt)
                break;
        }
        msg = "Search and sort done in "+getElapTimeStg(tm.elapsed());
        infoText->appendPlainText(msg);

    } else {
        msg = "ICAO "+icao+" not found in airport list";
        infoText->setPlainText(msg);
    }

}

void searchDialog::on_icao_changed(QString icao)
{
    QString style("color:red");
    if (icao.length() && main && main->airdata && main->airdata->findAirportByICAO(icao)) {
        if (!icao_list.contains(icao))
            icao_list += icao;
        style = "color:green";
    }
    icaoLab->setStyleSheet(style);
}


// eof - searchDialog.cpp

