/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */

// loadAirports.h

#ifndef LOADAIRPORTS_H
#define LOADAIRPORTS_H
#include "app_config.h"
#include <QWidget>
#include <QString>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QDomDocument>
#include <QDomNodeList>
#include <QDomNode>
#include <QList>
#include <QStringList>
#include "utilities/workThread.h"

typedef struct tagRUNLOAD {
    QString id;
    double lat1,lon1,hdg1,lat2,lon2,hdg2;
}RUNLOAD, *PRUNLOAD;

typedef QList<RUNLOAD> RUNLIST;

typedef struct tagAIRLOAD {
    QString name;   // if known - need apt.dat.gz
    QString icao;   // ICAO
    QString path;   // found on path
    RUNLIST runlist;    // runway list
}AIRLOAD, *PAIRLOAD;

typedef QList<PAIRLOAD> LOADLIST;
typedef LOADLIST * PLOADLIST;

typedef struct tagXREADSTR {
    QString path;
    PLOADLIST pLoadList;
    int file_count, thresh_count, air_count, run_count;
    QStringList info;
    int xr_flag;
}XREADSTR, * PXREADSTR;

#define xrf_Debug1   0x0001
#define xrf_Debug2   0x0002

typedef struct tagXLOADITEM {
    WORKITEM work;  // *MUST* be first
    XREADSTR xrs;
}XLOADITEM, *PXLOADITEM;

#define apf_Append   0x0001

class loadAirports : public QObject
{
    Q_OBJECT
public:
    explicit loadAirports(QObject *parent = 0);
    ~loadAirports();
    QWidget *parent;
    QString last_path;
    bool done_threshold;
    int file_count, thresh_count, air_count, run_count, time_ms;
    void clear_list(PLOADLIST);

    QStringList getThresholdList(QString);

    int readThreshold(QString);
    int readThreshold_OLD(QString);
    void readOnThread(QString path, PLOADLIST pll = 0);
    int def_xr_flag;
    bool isThreadInFunction();
    int writeAirportFile(QString output, PLOADLIST pll = 0, int flag = 0); // 0 = success
    PLOADLIST getLoadList() { return &m_ll; }
    PXLOADITEM getLoadItem() { return &m_xli; }

signals:
    void on_thread_done();

public slots:
    void readThreadDone(int,int);

private:
    LOADLIST m_ll;
    XLOADITEM m_xli;
    workThread *workthread;
    static void _loadThreshold(void *vp);

};


#endif // LOADAIRPORTS_H
