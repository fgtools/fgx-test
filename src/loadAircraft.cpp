// loadAircraft.cpp

#include "loadAircraft.h"
#include "mainwindow.h"

loadAircraft::loadAircraft(QObject *par)
{
    main = (MainWindow *)par;
    pworkThread = 0;
    al.in_load = FALSE;
    clear_list( getAeroListPtr() );
    next_aero = 0;
}

loadAircraft::~loadAircraft()
{
    if (pworkThread) {
        al.work.abort = true;

        delete pworkThread;
    }
    clear_list( getAeroListPtr() );
}


void loadAircraft::add2List(QString &directory, QString &aero, QString &xml_file, QString &description, QString &fdm,
                            QString &author, QString &file_path)
{
    PAEROITEM pai = new AEROITEM;
    pai->directory = directory;
    pai->aero = aero;
    pai->xml_file = xml_file;
    pai->description = description;
    pai->fdm = fdm;
    pai->author = author;
    pai->file_path = file_path;
    PAEROLIST pal = getAeroListPtr();
    pal->push_back(pai);
}

// static load (on a thread)
void loadAircraft::_loadStatic(void *vp)
{
    PAEROLOAD psAero = (PAEROLOAD)vp;
    QString path = psAero->dir;
    PAEROLIST pal = psAero->pal;
    // int flag = psAero->flag; // uses of this flag???
    QTime tm;
    QString next, msg;
    int found = 0;
    //= Get files Entries from Aircaft/ directory
    QDir aircraftDir(path);
    if (!aircraftDir.exists()) {
        outLog("loadAircraft::_loadStatic: directory "+path+" does NOT exist!");
        return;
    }

    next = "[3] loadAircraft::_loadStatic: Processing ["+path+"] for *-set.xml files...";
    outLog(next);
    tm.start();

    aircraftDir.setFilter( QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);

    QStringList entries = aircraftDir.entryList();
    //progress.setRange(0, entries.size() + 20);
    //next_dir = path;
    QStringList filters;
    filters << "*-set.xml";
    QStringList list_xml;
    QString xmlString;
    QDomDocument dom;
    QDomNodeList nodes;
    QDomNode n;
    int i;

    if (psAero->work.abort) {
        outLog("loadAircraft::_loadStatic: got abort request! (1)");
        return; // out of here
    }
    PAEROITEM pai = new AEROITEM;
    for( QStringList::ConstIterator item = entries.begin(); item != entries.end(); item++ ) {
        // watch for an ABORT request
        if (psAero->work.abort) {
            outLog("loadAircraft::_loadStatic: got abort request! (2)");
            break; // out of here
        }
        // Filter out default dir names, should be a QDir name filter?
        next = *item;
        if ((next != "Instruments") &&  (next != "Instruments-3d") && (next != "Generic")) {

            //progress.setValue(c);
            //progress.setLabelText(*entry);
            //progress.repaint();

            //** get the List of *-set.xml files in dir
            QDir dir( path + "/" + next );
            //QDir dir( mainObject->X->aircraft_path(*entry) );
            list_xml = dir.entryList(filters);
            if ( list_xml.count() > 0 ) { // << Scan Models
                //** Add Models
                for (i = 0; i < list_xml.size(); ++i) {
                    if (psAero->work.abort) {
                        outLog("loadAircraft::_loadStatic: got abort request! (3)");
                        break; // out of here
                    }
                    //** Add Path Node
                    pai->directory = next;
                    pai->xml_file = QString(list_xml.at(i));
                    pai->aero = QString(pai->xml_file);
                    pai->aero.chop(8);

                    //*=parse the Xml file - f&*& long winded
                    // QString file_path =  mainObject->X->aircraft_path(*entry);
                    pai->file_path = path + "/" + next;
                    pai->file_path.append("/");
                    pai->file_path.append(list_xml.at(i));
                    QFile xmlFile( pai->file_path );
                    if ( xmlFile.open(QIODevice::ReadOnly | QIODevice::Text) ) {
                        // outLog("File: "+pai->file_path);
                        /* The file content is converted to UTF-8.
                             Some files are Windows, encoding and throw error with QxmlQuery etc
                             Its a hack and don't quite understand whats happening.. said pedro
                        */
                        xmlString = QString(xmlFile.readAll()).toUtf8();
                        xmlFile.close();
                        if (psAero->work.abort) {
                            continue;
                        }
                        //QXmlQuery query;
                        //query.setFocus(xmlString);
                        //query.setFocus(&xmlFile); << Because file is not QTF8 using sting instead
                        //query.setQuery("PropertyList/sim");
                        //if ( query.isValid() ) {
                        //    QString res;
                        //    query.evaluateTo(&res);
                        //    // outLog(res);
                        //    dom.setContent("" + res + "");
                        QString errorMsg;
                        int errorLine = 0;
                        int errorColumn = 0;
                        if (dom.setContent(xmlString,&errorMsg,&errorLine,&errorColumn)) {
                            nodes = dom.elementsByTagName("sim");
                            n = nodes.at(0);
                            pai->description = n.firstChildElement("description").text();
                            pai->author = n.firstChildElement("author").text().trimmed().replace(("\n"),"");
                            pai->fdm = n.firstChildElement("flight-model").text();
                            if (psAero->work.abort) {
                                continue;
                            }
                            pal->push_back(pai);    // store this one
                            pai = new AEROITEM;
                            found++;
                        //} /* !query.isValid() */
                        //else
                        //{
                        //    outLog("Query is INVALID on file "+pai->file_path);
                        //}
                        } else {
                            outLog("dom.setContent FAILED on file "+pai->file_path);
                            msg.sprintf("Row=%d col=%d ",errorLine,errorColumn);
                            msg.append(errorMsg);
                            outLog("ERROR at "+msg);
                        }
                    } /*  xmlFile.open() */
                    else
                    {
                        outLog("Failed to open "+pai->file_path);
                    }

                    //QStringList lines;
                    //lines  << directory << aero << xml_file << description << fdm << author << file_path;
                    //out << lines.join("\t") << "\n";


                    //if(progress.wasCanceled()){
                    //	//qDebug() << "Progress cancelled!";
                    //	progress.hide();
                    //	return true;
                    //}
                    //c++;
                }

            } /* list_xml.count() > 0 */
        } /* entry != INstruments etc */
    } /* loop entries.() */
    delete pai;
    //cacheFile.close();
    //return false;
    if (!psAero->work.abort) {
        msg.sprintf("loadAircraft::_loadStatic: Found %d files, in ",found);
        msg.append(getElapTimeStg(tm.elapsed()));
        outLog(msg);
    }
    //writeAeroFile("/tmp/tempaero.txt", pal);
}

void loadAircraft::aero_thread_done(int wn, int ms)
{
    // hmmm, getting MULTIPLE callbacks???, so try
    disconnect(pworkThread,SIGNAL(work_done(int,int)),this,SLOT(aero_thread_done(int,int)));
    al.in_load = false;
    if (!al.work.abort) {
        QString msg;
        msg.sprintf("aero_thread_done: Done work %d, in ",wn);
        msg.append(getElapTimeStg(ms));
        main->setStatusMessage(msg);
    }
    emit aero_load_done();   // signal DONE work thread
}

PAEROLOAD loadAircraft::scanAircraft_on_thread(QString path, int flag, PAEROLIST pal)
{
    QString msg("loadAircraft::scanAircraft_on_thread: ");
    if (al.in_load) {
        msg.append("Already in LOAD!");
        outLog(msg);
        return &al;
    }
    al.in_load = true;
    al.dir = path;
    if (pal) {
        al.pal = pal;
        msg.append("using User 'pal' ");
    } else {
        al.pal = getAeroListPtr();
        msg.append("using class 'pal' ");
    }

    al.flag = flag;
    if ( al.flag & af_Append ) {
        msg.append("appending ");
    } else {
        clear_list(al.pal);
        msg.append("cleared list ");
    }
    if ( pworkThread ) {
        msg.append("current thread ");
    } else {
        pworkThread = new workThread(this);
        msg.append("new thread ");
    }

    al.work.user_type = 1;
    al.work.act = _loadStatic;  // (static) loader
    al.work.vp = &al;
    al.work.abort = false;
    connect(pworkThread,SIGNAL(work_done(int,int)),this,SLOT(aero_thread_done(int,int)));
    // al.tm.start();
    al.threadnum = pworkThread->work(_loadStatic,&al);   // start up the thread to do the work
    QString tmp;
    tmp.sprintf("Num %d",al.threadnum);
    msg.append(tmp);
    outLog(tmp);
    return &al;
}

bool loadAircraft::isThreadInFunction()
{
    if (pworkThread && pworkThread->in_function)
        return true;
    return false;
}


#if 0
void loadAircraft::scanAircraft_test(QString path)
{
    QString next;
    QTime tm;
    QString next_dir;
    int found = 0;
    PAEROLIST pal = getAeroListPtr();

    //= Get files Entries from Aircaft/ directory
    QDir aircraftDir(path);

    if (!aircraftDir.exists())
        return;

    current_path = path;        // could check if already done...
    clear_list(pal);    // re-start listing
    next = "[1] Processing folder "+path+" for *-set.xml files...";
    main->setStatusMessage(next);
    tm.start();

    aircraftDir.setFilter( QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);

    QStringList entries = aircraftDir.entryList();
    //progress.setRange(0, entries.size() + 20);
    next_dir = path;
    QStringList filters;
    filters << "*-set.xml";
    QStringList list_xml;
    //QString directory;
    //QString description;
    //QString author;
    //QString fdm;
    //QString xml_file;
    //QString aero;
    //QString file_path;
    QString xmlString;
    QDomDocument dom;
    QDomNodeList nodes;
    QDomNode n;
    int i;

    PAEROITEM pai = new AEROITEM;
    for( QStringList::ConstIterator item = entries.begin(); item != entries.end(); item++ ) {

        // Filter out default dir names, should be a QDir name filter?
        next = *item;
        if ((next != "Instruments") &&  (next != "Instruments-3d") && (next != "Generic")) {

            //progress.setValue(c);
            //progress.setLabelText(*entry);
            //progress.repaint();

            //** get the List of *-set.xml files in dir
            QDir dir( path + "/" + next );
            //QDir dir( mainObject->X->aircraft_path(*entry) );
            list_xml = dir.entryList(filters);

            if ( list_xml.count() > 0 ) { // << Scan Models

                //** Add Models
                for (i = 0; i < list_xml.size(); ++i) {

                    //** Add Path Node
                    pai->directory = next;
                    pai->xml_file = QString(list_xml.at(i));
                    pai->aero = QString(pai->xml_file);
                    pai->aero.chop(8);

                    //*=parse the Xml file - f&*& long winded
                    // QString file_path =  mainObject->X->aircraft_path(*entry);
                    pai->file_path = path + "/" + next;
                    pai->file_path.append("/");
                    pai->file_path.append(list_xml.at(i));
                    QFile xmlFile( pai->file_path );
                    if ( xmlFile.open(QIODevice::ReadOnly | QIODevice::Text) ) {
                        // outLog("File: "+pai->file_path);
                        /* The file content is converted to UTF-8.
                             Some files are Windows, encoding and throw error with QxmlQuery etc
                             Its a hack and don't quite understand whats happening.. said pedro
                        */
                        xmlString = QString(xmlFile.readAll()).toUtf8();
                        xmlFile.close();
                        QXmlQuery query;
                        query.setFocus(xmlString);
                        //query.setFocus(&xmlFile); << Because file is not QTF8 using sting instead
                        query.setQuery("PropertyList/sim");
                        if ( query.isValid() ) {

                            QString res;
                            query.evaluateTo(&res);
                            // outLog(res);
                            dom.setContent("" + res + "");
                            nodes = dom.elementsByTagName("sim");
                            n = nodes.at(0);
                            pai->description = n.firstChildElement("description").text();
                            pai->author = n.firstChildElement("author").text().trimmed().replace(("\n"),"");
                            pai->fdm = n.firstChildElement("flight-model").text();
                            //add2List(directory, aero, xml_file, description, fdm, author, file_path);
                            pal->push_back(pai);    // store this one
                            pai = new AEROITEM;
                            found++;
                        } /* !query.isValid() */
                        else
                        {
                            outLog("Query is INVALID on file "+pai->file_path);
                        }
                    } /*  xmlFile.open() */
                    else
                    {
                        outLog("Failed to open "+pai->file_path);
                    }

                    //QStringList lines;
                    //lines  << directory << aero << xml_file << description << fdm << author << file_path;
                    //out << lines.join("\t") << "\n";


                    //if(progress.wasCanceled()){
                    //	//qDebug() << "Progress cancelled!";
                    //	progress.hide();
                    //	return true;
                    //}
                    //c++;
                }

            } /* list_xml.count() > 0 */
        } /* entry != INstruments etc */
    } /* loop entries.() */
    delete pai;
    //cacheFile.close();
    //return false;
    next.sprintf("Found %d files, in ",found);
    next.append(getElapTimeStg(tm.elapsed()));
    main->setStatusMessage(next);
    writeAeroFile("/tmp/tempaero2.txt", pal);
}

void loadAircraft::scanAircraft(QString path)
{
    QString next;
    QTime tm;
    QString next_dir;
    int found = 0;
    PAEROLIST pal = getAeroListPtr();
    //= Get files Entries from Aircaft/ directory
    QDir aircraftDir(path);

    if (!aircraftDir.exists())
        return;

    current_path = path;        // could check if already done...
    clear_list(pal);    // re-start listing
    next = "[2] Processing folder "+path+" for *-set.xml files...";
    main->setStatusMessage(next);
    tm.start();

    aircraftDir.setFilter( QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);

    QStringList entries = aircraftDir.entryList();
    //progress.setRange(0, entries.size() + 20);
    next_dir = path;
    QStringList filters;
    filters << "*-set.xml";
    QStringList list_xml;
    //QString directory;
    //QString description;
    //QString author;
    //QString fdm;
    //QString xml_file;
    //QString aero;
    //QString file_path;
    QString xmlString;
    QDomDocument dom;
    QDomNodeList nodes;
    QDomNode n;
    int i;

    PAEROITEM pai = new AEROITEM;
    for( QStringList::ConstIterator item = entries.begin(); item != entries.end(); item++ ) {

        // Filter out default dir names, should be a QDir name filter?
        next = *item;
        if ((next != "Instruments") &&  (next != "Instruments-3d") && (next != "Generic")) {

            //progress.setValue(c);
            //progress.setLabelText(*entry);
            //progress.repaint();

            //** get the List of *-set.xml files in dir
            QDir dir( path + "/" + next );
            //QDir dir( mainObject->X->aircraft_path(*entry) );
            list_xml = dir.entryList(filters);
            if ( list_xml.count() > 0 ) { // << Scan Models
                //** Add Models
                for (i = 0; i < list_xml.size(); ++i) {
                    //** Add Path Node
                    pai->directory = next;
                    pai->xml_file = QString(list_xml.at(i));
                    pai->aero = QString(pai->xml_file);
                    pai->aero.chop(8);

                    //*=parse the Xml file - f&*& long winded
                    // QString file_path =  mainObject->X->aircraft_path(*entry);
                    pai->file_path = path + "/" + next;
                    pai->file_path.append("/");
                    pai->file_path.append(list_xml.at(i));
                    QFile xmlFile( pai->file_path );
                    if ( xmlFile.open(QIODevice::ReadOnly | QIODevice::Text) ) {
                        // outLog("File: "+pai->file_path);
                        /* The file content is converted to UTF-8.
                             Some files are Windows, encoding and throw error with QxmlQuery etc
                             Its a hack and don't quite understand whats happening.. said pedro
                        */
                        xmlString = QString(xmlFile.readAll()).toUtf8();
                        xmlFile.close();
                        QXmlQuery query;
                        query.setFocus(xmlString);
                        //query.setFocus(&xmlFile); << Because file is not QTF8 using sting instead
                        //query.setQuery("PropertyList/sim");
                        //if ( query.isValid() ) {

                            QString res(xmlString);
                        //    query.evaluateTo(&res);
                        //    // outLog(res);
                            dom.setContent("" + res + "");
                            nodes = dom.elementsByTagName("sim");
                            n = nodes.at(0);
                            pai->description = n.firstChildElement("description").text();
                            pai->author = n.firstChildElement("author").text().trimmed().replace(("\n"),"");
                            pai->fdm = n.firstChildElement("flight-model").text();
                            //add2List(directory, aero, xml_file, description, fdm, author, file_path);
                            pal->push_back(pai);    // store this one
                            pai = new AEROITEM;
                            found++;
                        //} /* !query.isValid() */
                        //else
                        //{
                        //    outLog("Query is INVALID on file "+pai->file_path);
                        //}
                    } /*  xmlFile.open() */
                    else
                    {
                        outLog("Failed to open "+pai->file_path);
                    }

                    //QStringList lines;
                    //lines  << directory << aero << xml_file << description << fdm << author << file_path;
                    //out << lines.join("\t") << "\n";


                    //if(progress.wasCanceled()){
                    //	//qDebug() << "Progress cancelled!";
                    //	progress.hide();
                    //	return true;
                    //}
                    //c++;
                }

            } /* list_xml.count() > 0 */
        } /* entry != INstruments etc */
    } /* loop entries.() */
    delete pai;
    //cacheFile.close();
    //return false;
    next.sprintf("Found %d files, in ",found);
    next.append(getElapTimeStg(tm.elapsed()));
    main->setStatusMessage(next);
    writeAeroFile("/tmp/tempaero.txt", pal);

}
#endif // 0

void loadAircraft::clear_list(PAEROLIST pal)
{
    if (pal) {
        int i, max;
        max = pal->count();
        for (i = 0; i < max; i++) {
            PAEROITEM pai = pal->at(i);
            delete pai;
        }
        pal->clear();
    }
}

int loadAircraft::writeAeroFile(QString file, PAEROLIST pal, QString sep, int flag)
{
    //QString file("/tmp/tempaero.txt");
    //PAEROLIST pal = &aeroList;
    //QString sep("\t");
    int count = pal->count();
    int i;
    if (!count) {
        outLog("WARNING: No list count to write to "+file);
        return count;
    }
    QFile data(file);
    QFile::OpenMode mode = (QIODevice::ReadWrite | QFile::Text);
    if ( !(flag & omf_Append) ) {
        mode |= QIODevice::Truncate;
    }
    //if (data.open(QIODevice::ReadWrite | QIODevice::Truncate | QFile::Text)) {
    if ( data.open(mode) ) {
        QTextStream out(&data);
        for (i = 0; i < count; i++) {
            PAEROITEM pai = pal->at(i);
            out << pai->directory;
            out << sep;
            out << pai->aero;
            out << sep;
            out << pai->xml_file;
            out << sep;
            out << pai->description;
            out << sep;
            out << pai->fdm;
            out << sep;
            out << pai->author;
            out << sep;
            out << pai->file_path;
            out << "\n";
        }
        data.close();
        outLog("List written to "+file+"!");
    } else {
        outLog("Failed to write "+file+"!");
    }
    return count;
}

int loadAircraft::loadAeroFile(QString file, PAEROLIST pal)
{
    QTime tm;
    QFile dataFile(file);
    QString msg;
    msg = "Loading cache "+file;
    main->setStatusMessage(msg);
    tm.start();
    if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        msg = "Load of "+file+" FAILED!";
        main->setStatusMessage(msg);
        return 0;
    }

    QTextStream in(&dataFile);
    QString line = in.readLine();
    QString last_dir("");
    QStringList cols;
    int c = 0;
    int done = 0;
    while( !line.isNull() ) {
        cols = line.split("\t");
        if (cols.count() >= C_MAX_COLS) {
            PAEROITEM pai = new AEROITEM;
            pai->directory = cols.at(C_DIR);
            pai->aero = cols.at(C_AERO);
            pai->xml_file = cols.at(C_RATING);
            pai->description = cols.at(C_DESCRIPTION);
            pai->fdm = cols.at(C_FDM);
            pai->author = cols.at(C_AUTHOR);
            pai->file_path = cols.at(C_FILE_PATH);
            if (pai->directory == last_dir) {
                // in same root directory
            } else {
                last_dir = pai->directory;
            }
            pal->push_back(pai);
            done++;
        } else {
            outLog("Line ["+line+"] did not split correctly!");
        }
        c++;
        line = in.readLine();
    }
    msg.sprintf("Done %d lines, for %d aircraft in ",c, pal->count());
    msg.append(getElapTimeStg(tm.elapsed()));
    main->setStatusMessage(msg);
    return done;
}

PAEROITEM loadAircraft::getFirstAero()
{
    PAEROLIST pal = getAeroListPtr();
    if (pal->count()) {
        next_aero = 0;
        PAEROITEM pai = pal->at(next_aero++);
        return pai;
    }
    return 0;
}

PAEROITEM loadAircraft::getNextAero()
{
    PAEROLIST pal = getAeroListPtr();
    if (next_aero < pal->count()) {
        PAEROITEM pai = pal->at(next_aero++);
        return pai;
    }
    return 0;
}

// eof - loadAircraft.cpp
