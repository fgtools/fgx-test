/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */

// loadAirports.cpp

#include "loadAirports.h"
#include "utilities/utilities.h"

loadAirports::loadAirports(QObject *parent) :
        QObject(parent)
{
    parent = (QWidget *)parent;
    last_path = "";
    done_threshold = false;
    clear_list(getLoadList());
    workthread = 0;
    def_xr_flag = xrf_Debug1;
    time_ms = 0;    // last time to completion on thread
}

loadAirports::~loadAirports()
{
    if (workthread) {
        m_xli.work.abort = true;   // set abort
        if (workthread->in_function) {
            // need to wait a bit
            int msecs = 0;  // wait for MAX 5 seconds
            QTime tm;
            tm.start();
            while (msecs < 5000) {
                msecs += 50;
                SleeperThread::msleep(50); // give up CPU
                if (!workthread->in_function)
                    break;
            }
            if (workthread->in_function) {
                outLog("loadAirports:XML: Workthread no aborted after "+getElapTimeStg(tm.elapsed())+", using 'terminate'!");
                workthread->was_terminated = true;
                workthread->terminate();
            } else {
                outLog("loadAirports:XML: Workthread aborted on request after "+getElapTimeStg(tm.elapsed()));
            }
        }
        delete workthread;
    }
    clear_list(getLoadList());
}

void loadAirports::clear_list(PLOADLIST pll)
{
    if (pll) {
        int i, max;
        max = pll->count();
        for (i = 0; i < max; i++) {
            PAIRLOAD pal = pll->at(i);
            delete pal;
        }
        pll->clear();
    }
}

QStringList loadAirports::getThresholdList(QString path)
{
    QTime tm;
    tm.start();
    QString msg;
    QStringList filter;
    filter << "*.threshold.xml";
    QStringList list = findFiles(path, filter, true);
    msg.sprintf("Got %d threshold.xml files",list.count());
    msg.append(", from path "+path+" in ");
    msg.append(getElapTimeStg(tm.elapsed()));
    outLog(msg);
    return list;
}

int loadAirports::readThreshold(QString path)
{
    QString msg;
    air_count = 0;
    PXLOADITEM pli = getLoadItem();
    PLOADLIST pll = getLoadList();
    if (util_isValidFGSceneryDir(path)) {
        path.append("/Airports");
        QDir d(path);
        if (d.exists()) {
            // looking good to go...
            pli->work.user_type = tht_loadAptXml;
            pli->work.act = _loadThreshold;  // (static) loader
            pli->work.vp = pli;
            pli->work.abort = false;
            PXREADSTR prs = &pli->xrs;
            prs->pLoadList = pll; // set the LIST to load
            prs->path = path;   // set the PATH to scan
            prs->file_count = prs->thresh_count = prs->air_count = prs->run_count = 0;
            prs->xr_flag = def_xr_flag;     // xrf_Debug1;
            _loadThreshold(pli);
            file_count = prs->file_count;
            thresh_count = prs->thresh_count;
            air_count = prs->air_count;
            run_count = prs->run_count;
            if (prs->info.count()) {
                for (int i = 0; i < prs->info.count(); i++) {
                    msg = prs->info.at(i);
                    outLog(msg);
                }
            }
        }   // found 'threshold.xml list in 'Airports' directory
        else {
            outLog("WARNING: No threshold.xml directories "+path);
        }
    }   // is valid scenery directory
    else {
        outLog("WARNING: Not valid scenery path "+path);
    }
    if (air_count) {
        done_threshold = true;
        last_path = path;
    }
    return air_count;
}

void loadAirports::readOnThread(QString path, PLOADLIST pll)
{
    QString msg;
    air_count = 0;
    PXLOADITEM pli = getLoadItem();
    if (util_isValidFGSceneryDir(path)) {
        path.append("/Airports");
        QDir d(path);
        if (d.exists()) {
            // looking good to go...
            pli->work.user_type = tht_loadAptXml;
            pli->work.act = _loadThreshold;  // (static) loader
            pli->work.vp = &m_xli;
            pli->work.abort = false;
            PXREADSTR prs = &pli->xrs;
            prs->pLoadList = (pll ? pll : getLoadList()); // set the LIST to load
            prs->path = path;   // set the PATH to scan
            prs->file_count = prs->thresh_count = prs->air_count = prs->run_count = 0;
            prs->xr_flag = def_xr_flag; // xrf_Debug1;
            // loadThreshold(&xli);
            // RUN THE THREAD
            if (!workthread) {
                workthread = new workThread();
            }
            connect(workthread,SIGNAL(work_done(int,int)),this,SLOT(readThreadDone(int,int)));
            workthread->work(_loadThreshold,pli);

        }   // found 'threshold.xml list in 'Airports' directory
        else {
            msg = "WARNING: No threshold.xml root directory ";
            msg.append(path);
            outLog(msg);
        }
    }   // is valid scenery directory
    else {
        msg = "WARNING: Not valid scenery path ";
        msg.append(path);
        outLog(msg);
    }
}

void loadAirports::readThreadDone(int num, int ms)
{
    QString msg;
    PXREADSTR prs = &m_xli.xrs;
    file_count = prs->file_count;
    thresh_count = prs->thresh_count;
    air_count = prs->air_count;
    run_count = prs->run_count;
    time_ms = ms;
    if (prs->info.count()) {
        for (int i = 0; i < prs->info.count(); i++) {
            msg = prs->info.at(i);
            outLog(msg);
        }
    }
    if (air_count) {
        done_threshold = true;
        last_path = prs->path;
        msg.sprintf("Work %d: ",num);
        msg.append(getElapTimeStg(ms));
        outLog(msg);
    }
    emit on_thread_done();  // send done to any connector
}

int loadAirports::readThreshold_OLD(QString path)
{
    QString msg;
    PLOADLIST pll = getLoadList();
    if (util_isValidFGSceneryDir(path)) {
        QString xFileName;
        int idxd, max, i, cnt;
        path.append("/Airports");
        QStringList list = getThresholdList(path);
        cnt = list.count();
        if (cnt) {
            // looking good to go...
            file_count = thresh_count = air_count = run_count = 0;
            //= Create domDocument - important dont pass string in  QDomConstrucor(string) as ERRORS.. took hours DONT DO IT
            QDomDocument dom;
            for (i = 0; i < cnt; i++) {
                //= Get file name
                xFileName = list.at(i);
                file_count++;
                //= Check if file entry is a *.threshold.xml - cos this is what we want
                // but now not really necessary since list is ONLY threshold.xml!!!
                // if(xFileName.endsWith(".threshold.xml") ) {

                    thresh_count++;
                    //= Split out "CODE.threshold.xml" with a "."
                    QFileInfo fileInfoThreshold(xFileName);
                    QString airport_code = fileInfoThreshold.fileName().split(".").at(0);
                    //==============================================================
                    // Parse the <CODE>.threshold.xml file to get runways
                    //==============================================================
                    /*
                    <?xml version="1.0"?>
                    <PropertyList>
                      <runway>
                        <threshold>
                          <lon>0.044298885776989</lon>
                          <lat>51.505569223906</lat>
                          <rwy>10</rwy>
                          <hdg-deg>92.88</hdg-deg>
                          <displ-m>95</displ-m>
                          <stopw-m>55</stopw-m>
                        </threshold>
                        <threshold>
                          <lon>0.065996952433288</lon>
                          <lat>51.5048897753222</lat>
                          <rwy>28</rwy>
                          <hdg-deg>272.88</hdg-deg>
                          <displ-m>71</displ-m>
                          <stopw-m>90</stopw-m>
                        </threshold>
                      </runway>
                    </PropertyList>
                    */

                    //* Get the contents of the file
                    // QString threshold_file( airport_dir.append("/").append(airport_code).append(".threshold.xml") );
                    QFile fileXmlThrehsold(xFileName);
                    if ( !fileXmlThrehsold.open(QIODevice::ReadOnly) ) {
                        outLog("WARNING: Failed to open "+xFileName);
                        continue;
                    }

                    //= Make file contents into a string from bytearray
                    QString xmlThresholdString = fileXmlThrehsold.readAll();

                    fileXmlThrehsold.close();   // done with file - close it...

                    if (xmlThresholdString.length() == 0) {
                        outLog("WARNING: Empty file "+xFileName);
                        continue;
                    }

                    if (!dom.setContent(xmlThresholdString)) { //* AFTER dom has been created, then set the content from a string from the file
                        outLog("WARNING: xml parsing error on file "+xFileName);
                        continue;
                    }

                    // we seem to have valid xml
                    // count a new airport
                    air_count++;
                    PAIRLOAD pal = new AIRLOAD;
                    pal->icao = airport_code;

                    QString fullpath = fileInfoThreshold.absoluteDir().absolutePath();
                    // outLog("Path: "+fullpath);
                    pal->path = fullpath;

                    //==================================
                    //= Get <runway> nodes
                    QDomNodeList nodeRunways = dom.elementsByTagName("runway");
                    max = nodeRunways.count();
                    run_count += max;
                    if (max > 0) {
                        for( idxd = 0; idxd < max; idxd++ ) {
                            // loops the <runway> nodes
                            RUNLOAD rl;
                            QDomNode nodeRunway = nodeRunways.at(idxd);

                            //= Runway threshold 0
                            QString rw1 = nodeRunway.childNodes().at(0).firstChildElement("rwy").text();
                            QString rw1lat = nodeRunway.childNodes().at(0).firstChildElement("lat").text();
                            QString rw1lon = nodeRunway.childNodes().at(0).firstChildElement("lon").text();
                            QString rw1hdg = nodeRunway.childNodes().at(0).firstChildElement("hdg-deg").text();

                            //= Runway threshold 1
                            QString rw2 = nodeRunway.childNodes().at(1).firstChildElement("rwy").text();
                            QString rw2lat = nodeRunway.childNodes().at(1).firstChildElement("lat").text();
                            QString rw2lon = nodeRunway.childNodes().at(1).firstChildElement("lon").text();
                            QString rw2hdg = nodeRunway.childNodes().at(1).firstChildElement("hdg-deg").text();

                            QString rwyid = rw1+"-"+rw2;
                            rl.id = rwyid;
                            rl.lat1 = rw1lat.toDouble();
                            rl.lon1 = rw1lon.toDouble();
                            rl.hdg1 = rw1hdg.toDouble();
                            rl.lat2 = rw2lat.toDouble();
                            rl.lon2 = rw2lon.toDouble();
                            rl.hdg2 = rw2hdg.toDouble();

                            pal->runlist.push_back(rl);

                            msg = airport_code;
                            msg.append(" ");
                            msg.append(rwyid);
                            msg.append(": ");
                            msg.append(rw1+" "+rw1lat+","+rw1lon+" "+rw1hdg);
                            msg.append(" ");
                            msg.append(rw2+" "+rw2lat+","+rw2lon+" "+rw2hdg);
                            outLog(msg);

                        }   // foreach 'runway' node
                    } else {
                        outLog("WARNING: No runway nodes in file "+xFileName);
                    }
                    pll->push_back(pal);
                // }   // deal with only threshold.xml - that is what we got
            }   // while filtered list has files
        }   // found 'threshold.xml list in 'Airports' directory
        else {
            outLog("WARNING: No threshold.xml files found on path "+path);
        }
    }   // is valid scenery directory
    else {
        outLog("WARNING: Not valid scenery path "+path);
    }
    if (air_count) {
        done_threshold = true;
        last_path = path;
    }
    return air_count;
}


void loadAirports::_loadThreshold(void *vp)
{
    PXLOADITEM pxli = (PXLOADITEM)vp;
    PXREADSTR pxr = &pxli->xrs; // get pointer to x load structure
    QString path(pxr->path);
    PLOADLIST pAirList = pxr->pLoadList;
    int flag = pxr->xr_flag;
    QString msg;
    QDomDocument dom;
    QString dot(".");
    QString dotdot("..");
    QString airport_code;
    QString xFileName;
    QString xfileExt(".threshold.xml");
    QDomNodeList nodeRunways;
    QDomNode nodeRunway;
    QString elrwy("rwy");
    QString ellat("lat");
    QString ellon("lon");
    QString elhdg("hdg_deg");
    QString idsep(" - ");
    //= Runway threshold 0
    QString rw1;    // = nodeRunway.childNodes().at(0).firstChildElement("rwy").text();
    QString rw1lat; // = nodeRunway.childNodes().at(0).firstChildElement("lat").text();
    QString rw1lon; // = nodeRunway.childNodes().at(0).firstChildElement("lon").text();
    QString rw1hdg; // = nodeRunway.childNodes().at(0).firstChildElement("hdg-deg").text();
    //= Runway threshold 1
    QString rw2;    // = nodeRunway.childNodes().at(1).firstChildElement("rwy").text();
    QString rw2lat; // = nodeRunway.childNodes().at(1).firstChildElement("lat").text();
    QString rw2lon; // = nodeRunway.childNodes().at(1).firstChildElement("lon").text();
    QString rw2hdg; // = nodeRunway.childNodes().at(1).firstChildElement("hdg-deg").text();
    int idxd, max;
    pxr->file_count = pxr->thresh_count = pxr->air_count = pxr->run_count = 0;
    // looking good to go...
    QDirIterator loopFiles( path, QDirIterator::Subdirectories );
    while (loopFiles.hasNext()) {
        if (pxli->work.abort)
            break;  // user requested abort
        //= Get file handle if there is one
        xFileName = loopFiles.next();
        if ((xFileName == dot)||(xFileName == dotdot))
            continue;   // forget these

        pxr->file_count++;
        //= Check if file entry is a *.threshold.xml - cos this is what we want
        if (xFileName.endsWith(xfileExt) ) { // ".threshold.xml"

            pxr->thresh_count++;
            //= Split out "CODE.threshold.xml" with a "."
            QFileInfo fileInfoThreshold(xFileName);
            airport_code = fileInfoThreshold.fileName().split(".").at(0);
            //==============================================================
            // Parse the <CODE>.threshold.xml file to get runways
            //==============================================================
            /*
            <?xml version="1.0"?>
            <PropertyList>
              <runway>
                <threshold>
                  <lon>0.044298885776989</lon>
                  <lat>51.505569223906</lat>
                  <rwy>10</rwy>
                  <hdg-deg>92.88</hdg-deg>
                  <displ-m>95</displ-m>
                  <stopw-m>55</stopw-m>
                </threshold>
                <threshold>
                  <lon>0.065996952433288</lon>
                  <lat>51.5048897753222</lat>
                  <rwy>28</rwy>
                  <hdg-deg>272.88</hdg-deg>
                  <displ-m>71</displ-m>
                  <stopw-m>90</stopw-m>
                </threshold>
              </runway>
            </PropertyList>
            */

            //* Get the contents of the file
            // QString threshold_file( airport_dir.append("/").append(airport_code).append(".threshold.xml") );
            QFile fileXmlThrehsold(xFileName);
            if ( !fileXmlThrehsold.open(QIODevice::ReadOnly) ) {
                msg = "WARNING: Failed to open ";
                msg.append(xFileName);
                pxr->info += msg;
                continue;
            }

            //= Make file contents into a string from bytearray
            QString xmlThresholdString = fileXmlThrehsold.readAll();

            fileXmlThrehsold.close();   // done with file - close it...

            if (xmlThresholdString.length() == 0) {
                msg = "WARNING: Empty file ";
                msg.append(xFileName);
                pxr->info += msg;
                continue;
            }

            if (flag & xrf_Debug2) {
                QString fullpath = fileInfoThreshold.absoluteDir().absolutePath();
                outLog("Path: "+fullpath);
            }

            //= Create domDocument - important dont pass string in  QDomConstrucor(string) as ERRORS.. took hours DONT DO IT
            dom.setContent(xmlThresholdString); //* AFTER dom has been created, then set the content from a string from the file

            //==================================
            //= Get <runway> nodes
            nodeRunways = dom.elementsByTagName("runway");
            max = nodeRunways.count();
            pxr->run_count += max;
            if (max > 0) {
                // count a new airport
                pxr->air_count++;
                PAIRLOAD pal = new AIRLOAD;
                pal->icao = airport_code;
                for( idxd = 0; idxd < max; idxd++ ) {
                    if (pxli->work.abort)
                        break;  // user requested abort
                    // loops the <runway> nodes
                    RUNLOAD rl;
                    nodeRunway = nodeRunways.at(idxd);

                    //= Runway threshold 0
                    rw1 = nodeRunway.childNodes().at(0).firstChildElement(elrwy).text();
                    rw1lat = nodeRunway.childNodes().at(0).firstChildElement(ellat).text();
                    rw1lon = nodeRunway.childNodes().at(0).firstChildElement(ellon).text();
                    rw1hdg = nodeRunway.childNodes().at(0).firstChildElement(elhdg).text();

                    //= Runway threshold 1
                    rw2 = nodeRunway.childNodes().at(1).firstChildElement(elrwy).text();
                    rw2lat = nodeRunway.childNodes().at(1).firstChildElement(ellat).text();
                    rw2lon = nodeRunway.childNodes().at(1).firstChildElement(ellon).text();
                    rw2hdg = nodeRunway.childNodes().at(1).firstChildElement(elhdg).text();

                    rl.id = rw1;
                    rl.id.append(idsep);
                    rl.id.append(rw2);
                    rl.lat1 = rw1lat.toDouble();
                    rl.lon1 = rw1lon.toDouble();
                    rl.hdg1 = rw1hdg.toDouble();
                    rl.lat2 = rw2lat.toDouble();
                    rl.lon2 = rw2lon.toDouble();
                    rl.hdg2 = rw2hdg.toDouble();

                    pal->runlist.push_back(rl);

                    if (flag & xrf_Debug1) {
                        msg = airport_code;
                        msg.append(" ");
                        msg.append(rl.id);
                        msg.append(": ");
                        msg.append(rw1+" "+rw1lat+","+rw1lon+" "+rw1hdg);
                        msg.append(" ");
                        msg.append(rw2+" "+rw2lat+","+rw2lon+" "+rw2hdg);
                        outLog(msg);
                    }

                }   // foreach 'runway' node
                pAirList->push_back(pal);
            } else {
                msg = "WARNING: No runway nodes in file ";
                msg.append(xFileName);
                pxr->info += msg;
            }
        }   // deal with only threshold.xml
    }   // while directory has files
}

bool loadAirports::isThreadInFunction()
{
    if (workthread && workthread->in_function)
        return true;
    return false;
}


int loadAirports::writeAirportFile(QString output, PLOADLIST pll, int flag)
{
    int i;
    if (!pll)
        return 1;   // no list
    int count = pll->count();
    if (count == 0)
        return 2;   // no count in list
    if (output.isEmpty())
        return 3;   // no file given

    QFile file(output);    // set output file
    QFile::OpenMode mode = (QIODevice::ReadWrite | QFile::Text);
    if ( !(flag & apf_Append) ) {
        mode |= QIODevice::Truncate;
    }
    if ( !file.open(mode) )
        return 4;   // failed open/create/truncate file
    // ready to write - what to write
    QTextStream out(&file);
    int pathnumber = 0;
    for (i = 0; i < count; i++) {
        pathnumber++;
        PAIRLOAD pal = pll->at(i);
        QStringList cols; // missing in middle is description ??
        cols << pal->icao << pal->name << pal->path << QString::number(pathnumber);
        out << cols.join("\t").append("\n");
    }
    file.close();
    return 0;    // file written (or appended)
}

// eof - loadAirports.cpp
