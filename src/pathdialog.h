/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   ================================================== */
#ifndef PATHDIALOG_H
#define PATHDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QDir>
#include <QListWidget>
#include <QListWidgetItem>
#include <QAbstractItemView>
#include <QFont>
#include <QFontMetrics>

class MainWindow;

class pathDialog : public QDialog
{
    Q_OBJECT
public:
    explicit pathDialog(QWidget *parent = 0);

    // in case user want to change the info
    QLabel *info;   // = new QLabel("Get Flightgear root and scenery paths",this);
    // or the labels
    QLabel *rootLab;        // = new QLabel("FG_ROOT:",this);
    QLabel *sceneLab;       // = new QLabel("Scenery:",this);

    bool root_valid;
    bool scene_valid;
    bool exit_ok;

    void init(QString root, QString scene,
              QStringList lroot = QStringList(), QStringList lscene = QStringList());

    // get results
    QString get_root() { return rootEd->text(); }
    QString get_scene() { return sceneEd->text(); }

signals:
    void on_user_ok(QString,QString);   // emitted on user clicking OK

public slots:
    void on_cancel();
    void on_ok();
    void on_browse_root();
    void on_browse_scene();
    void on_scene_changed(QString);
    void on_root_changed(QString);
    void on_root_selection();
    void on_scene_selection();

private:

    QWidget *parent_not_used;  // no care about OWNER

    QGridLayout *mainLayout;

    QLineEdit *rootEd;      // = new QLineEdit(this);
    QPushButton *rootBut;   // = new QPushButton("...",this);

    QListWidget *rootList;     // = new QListWidget(this);

    QLineEdit *sceneEd;     // = new QLineEdit(this);
    QPushButton *sceneBut;  // = new QPushButton("...",this);

    QListWidget *sceneList;     // = new QListWidget(this);

    QPushButton *cancelBut; // = new QPushButton("Cancel",this);
    QPushButton *okBut;     // = new QPushButton("Ok",this);

    // now just redirections to the utilities functions
    bool is_valid_fg_root(QString);
    bool is_valid_fg_scenery(QString);

};

#endif // PATHDIALOG_H
