#ifndef AIRCRAFTWIDGET_H
#define AIRCRAFTWIDGET_H

#include "app_config.h"
#ifdef ADD_TAB_WIDGET  // make the center a TAB widget

#include <QObject>
#include <QWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QStatusBar>
#include <QLabel>
#include <QCheckBox>
#include <QMutex>

#include "loadAircraft.h"

class MainWindow;

enum TREE_VIEWS {
    LIST_VIEW,
    FOLDER_VIEW
};

class AircraftWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AircraftWidget(QObject *parent = 0);
    ~AircraftWidget();
    MainWindow *main;
    QTreeWidget *treeWidget;
    QStatusBar *statusBarTree;
    QLabel *labelAeroPath;
    QCheckBox *checkViewNested;
    bool select_node(QString aero);
    QString selected_aircraft();
    int load_tree_from_list(PAEROLIST pal);

public slots:
    void on_tree_selection_changed();
    int load_tree();

private:
    QMutex m_treeMutex;
    void lockTreeMutex() { m_treeMutex.lock();  } // grap the mutex
    void unlockTreeMutex()  { m_treeMutex.unlock(); } // release the mutex

};


#endif // #define ADD_TAB_WIDGET  // make the center a TAB widget

#endif // AIRCRAFTWIDGET_H
// eof - aircraftWidget.h
