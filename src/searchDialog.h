/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   ================================================== */
#ifndef SEARCHDIALOG_H
#define SEARCHDIALOG_H
#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QLineEdit>
#include <QCheckBox>
#include <QStringList>

class MainWindow;

class searchDialog : public QDialog
{
    Q_OBJECT
public:
    explicit searchDialog(QWidget *parent = 0);

    MainWindow *main;

    QGridLayout *mainLayout;    // = new QGridLayout(this);

    QLabel *info;   // = new QLabel("Search for airport ICAO, and get closest airports",this);

    QLabel *icaoLab;    // = new QLabel("ICAO:",this);
    QLineEdit *icaoEd;  // = new QLineEdit(this);
    QLabel *cntLab;     // = new QLabel("Count:",this);
    QLineEdit *cntEd;   // = new QLineEdit(this);

    QPlainTextEdit *infoText;   // = new QPlainTextEdit(this);

    QPushButton *srchBut;   // = new QPushButton(this);
    QPushButton *okBut;     // = new QPushButton(this);
    QStringList icao_list;

signals:

public slots:
    void on_search();
    void on_done();
    void on_icao_changed(QString);

private:
    // checkbox options
    QLabel *xcludLab;       // = new QLabel("Exclude:",this);
    QCheckBox *xheliCheck;  // = new QCheckBox("Heli",this);
    QCheckBox *xseaCheck;   // = new QCheckBox("Sea",this);
    QCheckBox *xclosedCheck;// = new QCheckBox("Closed",this);
    QLabel *detailLab;
    QCheckBox *runCheck;
    QCheckBox *commCheck;

};

#endif // SEARCHDIALOG_H
