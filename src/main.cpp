/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Sep 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */

#include "app_config.h"
#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setOrganizationDomain("geoffair.org");

    // store the UNIX file as $HOME/.config/geoffair/Qt_OSM_Map.conf - nav: . hconf; cd geoffair
    QApplication::setOrganizationName("geoffair");
    QApplication::setApplicationName(APP_SETN);
    QApplication::setApplicationVersion(APP_SETV);

    QString title(APP_NAME);
    title.append(" v");
    title.append(APP_VERS);

    MainWindow w;
    w.setWindowTitle(title);
    //w.setWindowIcon(QIcon("/icon/osgmap"));
    w.show();

    return a.exec();
}
