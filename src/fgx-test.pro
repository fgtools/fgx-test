#-------------------------------------------------
#
# Project created by QtCreator 2011-09-21T15:59:53
#
#-------------------------------------------------

QT += core gui
QT += network \
    xml \
    xmlpatterns \
    webkit

TARGET = fgx-test
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    osm_map.cpp \
    utilities/utilities.cpp \
    testdialog.cpp \
    fileDialog.cpp \
    moveDialog.cpp \
    dirDialog.cpp \
    pathdialog.cpp \
    utilities/workThread.cpp \
    loadAirports.cpp \
    utilities/zlib/zutil.c \
    utilities/zlib/uncompr.c \
    utilities/zlib/trees.c \
    utilities/zlib/inftrees.c \
    utilities/zlib/inflate.c \
    utilities/zlib/inffast.c \
    utilities/zlib/infback.c \
    utilities/zlib/gzwrite.c \
    utilities/zlib/gzread.c \
    utilities/zlib/gzlib.c \
    utilities/zlib/gzclose.c \
    utilities/zlib/deflate.c \
    utilities/zlib/crc32.c \
    utilities/zlib/compress.c \
    utilities/zlib/adler32.c \
    loadAptDat.cpp \
    utilities/fgx_gzlib.cpp \
    searchDialog.cpp \
    utilities/simgear/SGGeodesy.cpp \
    getAptDat.cpp \
    loadAircraft.cpp \
    aircraftWidget.cpp \
    airportsWidget.cpp \
    xwidgets/xgroupboxes.cpp

HEADERS  += mainwindow.h \
    osm_map.h \
    app_config.h \
    utilities/utilities.h \
    testdialog.h \
    fileDialog.h \
    moveDialog.h \
    dirDialog.h \
    pathdialog.h \
    utilities/workThread.h \
    loadAirports.h \
    utilities/zlib/trees.h \
    utilities/zlib/inftrees.h \
    utilities/zlib/inflate.h \
    utilities/zlib/inffixed.h \
    utilities/zlib/inffast.h \
    utilities/zlib/gzguts.h \
    utilities/zlib/fgx_zutil.h \
    utilities/zlib/fgx_zlib.h \
    utilities/zlib/fgx_zconf.h \
    utilities/zlib/deflate.h \
    utilities/zlib/crc32.h \
    loadAptDat.h \
    utilities/fgx_gzlib.h \
    searchDialog.h \
    getAptDat.h \
    utilities/simgear/SGGeodesy.h \
    utilities/simgear/compiler.h \
    utilities/simgear/constants.h \
    loadAircraft.h \
    aircraftWidget.h \
    airportsWidget.h \
    xwidgets/xgroupboxes.h

RESOURCES += \
    resources/openlayers.qrc \
    resources/ycons.qrc
