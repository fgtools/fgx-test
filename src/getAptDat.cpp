/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   ================================================== */
#include "getAptDat.h"
#include "fileDialog.h"
#include "mainwindow.h"

getAptDat::getAptDat(QWidget *parent) :
    QDialog(parent)
{
    main = (MainWindow *)parent;
    setWindowTitle("GET DATA FILES");
    setMinimumWidth(550);
    got_ok = false;
    int m = 10;
    int row = 0;
    int cols = 10;
    //QString msg;

    mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(m,m,m,m);
    mainLayout->setSpacing(10);

    setLayout(mainLayout);

    infoLab = new QLabel("Enter or search for data files",this);
    mainLayout->addWidget(infoLab,row,0,1,cols);
    row++;

    aptdatLab = new QLabel("FG Apt.dat:",this);
    aptdatLab->setAlignment(Qt::AlignRight);
    aptdatEd = new QLineEdit(this);
    aptdatBut = new QPushButton("...",this);

    mainLayout->addWidget(aptdatLab,row,0,1,1);
    mainLayout->addWidget(aptdatEd,row,1,1,cols-2);
    mainLayout->addWidget(aptdatBut,row,cols-1,1,1);
    row++;

    connect(aptdatBut,SIGNAL(clicked()),this,SLOT(on_get_aptdat()));
    connect(aptdatEd,SIGNAL(textChanged(QString)),this,SLOT(on_aptdat_changed(QString)));

    aptdatList = new QListWidget(this);
    aptdatList->setSelectionMode(QAbstractItemView::SingleSelection);
    aptdatList->setSelectionBehavior(QAbstractItemView::SelectRows);
    mainLayout->addWidget(aptdatList,row,1,3,cols-2);
    row += 3;

    connect(aptdatList,SIGNAL(itemSelectionChanged()),this,SLOT(on_aptdat_selection()));

    okBut = new QPushButton("Ok",this);
    cancelBut = new QPushButton("Cancel",this);
    mainLayout->addWidget(cancelBut,row,cols-2,1,1);
    mainLayout->addWidget(okBut,row,cols-1,1,1);

    connect(okBut,SIGNAL(clicked()),this,SLOT(on_ok()));
    connect(cancelBut,SIGNAL(clicked()),this,SLOT(on_cancel()));

}

void getAptDat::setup(QString in, QStringList list)
{
    QString prev(in);
    util_ensureUnixPathSep(prev);
    aptdatEd->setText(prev);
    int i, cnt, rows, j;
    int fh = 16;    // gestimate font height (pixels)
    int mrows = 5;  // maximim rows
    int rowadj = 2; // an adjustment, for the boder region
    QString tmp;

    QFont font = aptdatList->font();
    QFontMetrics met(font);
    fh = met.height();
    if (fh < 16)
        fh = 16;
    j = aptdatList->sizeHintForRow(0);
    if (j > fh)
        fh = j;

    // add what user gave
    // deal with any root list given
    rows = 0;
    aptdatList->clear();
    if (prev.length()) {
        new QListWidgetItem(prev,aptdatList);
        aptdatList->item(0)->setSelected(true);
        j = aptdatList->sizeHintForRow(rows);
        if (j > fh)
            fh = j;
        rows++;
    }
    cnt = list.count();
    for (i = 0; i < cnt; i++) {
        tmp = list.at(i);         // add to list
        if (tmp.length() && (tmp != prev)) {
            util_ensureUnixPathSep(tmp);
            new QListWidgetItem(tmp,aptdatList);
            j = aptdatList->sizeHintForRow(rows);
            if (j > fh)
                fh = j;
            rows++;
        }
    }

    // moderate rows value
    if (rows > mrows)
        rows = mrows;
    else if (rows < 2)
        rows = 2;
    else
        rows++; // plus one blank
    aptdatList->setMaximumHeight((rows*fh)+(rows*rowadj));

    on_aptdat_changed(prev);    // set the color indicator
}


void getAptDat::on_ok()
{
    QFile file(aptdatEd->text());
    if (file.exists())
        got_ok = true;
    close();
}

void getAptDat::on_cancel()
{
    close();
}

void getAptDat::on_get_aptdat()
{
    QString title(tr("Get Existing apt.dat File"));
    QString prev(aptdatEd->text());
    fileDialog d(this);
    d.init(title,prev);
    d.exec();
    if (!d.got_cancel) {
        QString nfile = d.getFileName();
        QFile f(nfile);
        if (nfile.length() && f.exists() && (nfile != prev)) {
            aptdatEd->setText(nfile);
        }
    }

}

void getAptDat::on_aptdat_selection()
{
    QListWidgetItem *item = aptdatList->currentItem();
    if (!item)
        return;
    QString path = item->text();
    if (path.length()) {
        aptdatEd->setText(path);
        on_aptdat_changed(path);
    }
}

void getAptDat::on_aptdat_changed(QString txt)
{
    QString style("color:red");
    if (txt.length()) {
        QString tmp(txt);
        if (util_ensureUnixPathSep(tmp)) {
            aptdatEd->setText(tmp);
        }
        QFile file(tmp);
        QDir dir(tmp);
        if (file.exists() && !dir.exists()) {
            style = "color:green";
        }
    }
    aptdatLab->setStyleSheet(style);
}

// eof - getAptDat.cpp
