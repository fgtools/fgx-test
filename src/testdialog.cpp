/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Sep 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */

#include "testdialog.h"
#include "osm_map.h"        // for min/max slider range
#include "mainwindow.h"
#include "utilities/utilities.h"    // outLog()

testDialog::testDialog(QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle("TEST DIALOG");

    m_Options = 0;
    main = (MainWindow *)parent;

    int m = 10;
    int row = 0;

    mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(m,m,m,m);
    mainLayout->setSpacing(10);
    setLayout(mainLayout);

    info = new QLabel("Testing Native get Directory and File",this);
    mainLayout->addWidget(info,row,0,1,4);
    row++;

    gdLab = new QLabel("Directory:",this);
    gdEd = new QLineEdit(this);
    mainLayout->addWidget(gdLab,row,0,1,1);
    mainLayout->addWidget(gdEd,row,1,1,3);
    gdEd->setText(main->settings->value(S_NATDIR,"").toString());
    row++;

    getDir = new QPushButton(this);
    getDir->setText("Get Directory");
    mainLayout->addWidget(getDir,row,0,1,4);
    connect(getDir,SIGNAL(clicked()),this,SLOT(on_get_dir()));
    row++;

    gfLab = new QLabel("File:",this);
    gfEd = new QLineEdit(this);
    gfEd->setText(main->settings->value(S_NATFILE,"").toString());
    mainLayout->addWidget(gfLab,row,0,1,1);
    mainLayout->addWidget(gfEd,row,1,1,3);
    row++;

    fiLab = new QLabel("Filter:",this);
    fiEd = new QLineEdit(this);
    mainLayout->addWidget(fiLab,row,0,1,1);
    mainLayout->addWidget(fiEd,row,1,1,3);
    row++;

    opLab = new QLabel("Options:",this);

    optVal = new QLineEdit(this);
    sdo = new QCheckBox("ShowDirsOnly",this); // if (options & 0x0001)
    drsl = new QCheckBox("DontResolveSymlinks",this); // if (options & 0x0002)
    dcow = new QCheckBox("DontConfirmOverwrite",this); // if (options & 0x0004)

    dus = new QCheckBox("DontUseSheet",this); // if (options & 0x0008)
    dund = new QCheckBox("DontUseNativeDialog",this); // if (options & 0x0010)
    ro = new QCheckBox("ReadOnly",this); // if (options & 0x0020)
    hnfd = new QCheckBox("HideNameFilterDetails",this); // if (options & 0x0040)

    mainLayout->addWidget(opLab,row,0,1,1);
    row++;

    mainLayout->addWidget(optVal,row,0,1,1);
    mainLayout->addWidget(sdo,row,1,1,1);
    mainLayout->addWidget(drsl,row,2,1,1);
    mainLayout->addWidget(dcow,row,3,1,1);
    row++;

    mainLayout->addWidget(dus,row,0,1,1);
    mainLayout->addWidget(dund,row,1,1,1);
    mainLayout->addWidget(ro,row,2,1,1);
    mainLayout->addWidget(hnfd,row,3,1,1);
    row++;

    connect(sdo,SIGNAL(clicked(bool)),this,SLOT(on_sdo(bool)));
    connect(drsl,SIGNAL(clicked(bool)),this,SLOT(on_drsl(bool)));
    connect(dcow,SIGNAL(clicked(bool)),this,SLOT(on_dcow(bool)));
    connect(dus,SIGNAL(clicked(bool)),this,SLOT(on_dus(bool)));
    connect(dund,SIGNAL(clicked(bool)),this,SLOT(on_dund(bool)));
    connect(ro,SIGNAL(clicked(bool)),this,SLOT(on_ro(bool)));
    connect(hnfd,SIGNAL(clicked(bool)),this,SLOT(on_hnfd(bool)));

    getFile = new QPushButton("Get File Name",this);
    getNFile = new QPushButton("Get New File",this);
    mainLayout->addWidget(getFile,row,0,1,2);
    mainLayout->addWidget(getNFile,row,2,1,2);
    connect(getFile,SIGNAL(clicked()),this,SLOT(on_get_file()));
    connect(getNFile,SIGNAL(clicked()),this,SLOT(on_get_nfile()));
    row++;


    row++;
    ok = new QPushButton(this);
    ok->setText("Done");
    mainLayout->addWidget(ok,row,1,1,2);
    connect(ok, SIGNAL(clicked()), this, SLOT(on_ok()));
    m_Options = main->settings->value(S_NATOPTS,0).toInt();

    set_optVal();
}

void testDialog::set_optVal()
{
    QString msg;
    msg.sprintf("%d",m_Options);
    optVal->setText(msg);
    main->settings->setValue(S_NATOPTS,m_Options);
}

void testDialog::on_ok()
{
    close();
}

void testDialog::on_sdo(bool b)
{
    // = new QCheckBox("ShowDirsOnly",this); // if (options & 0x0001)
    if (b)
        m_Options |= 0x0001;
    else
        m_Options &= ~(0x0001);
    set_optVal();
}
void testDialog::on_drsl(bool b)
{
    //= new QCheckBox("DontResolveSymlinks",this); // if (options & 0x0002)
    if (b)
        m_Options |= 0x0002;
    else
        m_Options &= ~(0x0002);
    set_optVal();
}

void testDialog::on_dcow(bool b)
{
    //= new QCheckBox("DontConfirmOverwrite",this); // if (options & 0x0004)
    if (b)
        m_Options |= 0x0004;
    else
        m_Options &= ~(0x0004);
    set_optVal();
}
void testDialog::on_dus(bool b)
{
    //= new QCheckBox("DontUseSheet",this); // if (options & 0x0008)
    if (b)
        m_Options |= 0x0008;
    else
        m_Options &= ~(0x0008);
    set_optVal();
}
void testDialog::on_dund(bool b)
{
    // = new QCheckBox("DontUseNativeDialog",this); // if (options & 0x0010)
    if (b)
        m_Options |= 0x0010;
    else
        m_Options &= ~(0x0010);
    set_optVal();
}
void testDialog::on_ro(bool b)
{
    // = new QCheckBox("ReadOnly",this); // if (options & 0x0020)
    if (b)
        m_Options |= 0x0020;
    else
        m_Options &= ~(0x0020);
    set_optVal();
}
void testDialog::on_hnfd(bool b)
{
    //= new QCheckBox("HideNameFilterDetails",this); // if (options & 0x0040)
    if (b)
        m_Options |= 0x0040;
    else
        m_Options &= ~(0x0040);
    set_optVal();
}

void testDialog::on_get_dir()
{
    QString dir(gdEd->text());
    dir = util_browseDirectory(this,"Get Directory",dir);
    gdEd->setText(dir);

    main->settings->setValue(S_NATDIR,dir);
    main->setStatusMessage("Got dir: "+dir,DEF_TIMEOUT);
}
// QString util_browseFile(QWidget * parent = 0,
//                         QString prompt = QString(),
//                         QString current = QString(),
//                         QStringList filter = QStringList(),
//                         int options = 0);
void testDialog::on_get_file()
{
    QString prompt = "Select File";
    QString current = gfEd->text();
    QString filt = fiEd->text();
    QStringList filter = filt.split(" ",QString::SkipEmptyParts);
    current = util_browseFile(this, prompt, current, filter, m_Options);
    //if (current.length()) {
        gfEd->setText(current);
        main->settings->setValue(S_NATFILE,current);
    //}
    main->setStatusMessage("Got File: "+current,DEF_TIMEOUT);
}
// QString util_browseNewFile(QWidget * parent = 0,
//                        QString prompt = QString(),
//                        QString current = QString(),
//                        QStringList filter = QStringList(),
//                        int options = 0);
void testDialog::on_get_nfile()
{
    QString prompt = "Get NEW File Name";
    QString current = gfEd->text();
    QString filt = fiEd->text();
    QStringList filter = filt.split(" ",QString::SkipEmptyParts);
    current = util_browseNewFile(this, prompt, current, filter, m_Options);
    //if (current.length()) {
        gfEd->setText(current);
        main->settings->setValue(S_NATFILE,current);
    //}
    main->setStatusMessage("Got New File: "+current,DEF_TIMEOUT);
}

// eof - testdialog.cpp
