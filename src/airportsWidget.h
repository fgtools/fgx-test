#ifndef AIRPORTSWIDGET_H
#define AIRPORTSWIDGET_H

#include "app_config.h"
#include <QObject>
#include <QWidget>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QTreeView>
#include <QModelIndex>
#include <QSplitter>
#include <QStringList>
#include <QButtonGroup>
#include <QAbstractButton>
#include <QRadioButton>
#include <QString>
#include <QToolButton>
#include <QStatusBar>
#include <QLabel>
#include <QVBoxLayout>
#include <QTreeWidget>

#include "xwidgets/xgroupboxes.h"
#include "utilities/utilities.h"

class MainWindow;

class AirportsWidget : public QWidget
{
    Q_OBJECT
public:

    enum AIPORTS_TREE_COLS{
        CA_CODE = 0,
        CA_NAME = 1,
        CA_DIR = 2
    };

    enum AIRPORT_INFO_TREE_COLS{
        CI_NODE = 0,
        CI_LABEL = 1,
        CI_TYPE = 2,
        CI_SETTING_KEY = 3,
        CI_WIDTH = 4,
        CI_LENGTH  = 5,
        CI_LAT = 6,
        CI_LON = 7,
        CI_HEADING =8,
        CI_RUNWAYS = 9
        //CI_HDG = 10
    };

    enum STARTUP_POSTITION{
        USE_DEFAULT = 0,
        USE_AIRPORT = 1,
        USE_COORDINATES = 2
    };

    explicit AirportsWidget(QObject *parent);
    ~AirportsWidget();

    MainWindow *main;

    XGroupVBox *groupBoxAirport;    // = new XGroupVBox("Start at airport");
    QTabWidget *tabWidget;
    QStandardItemModel *model;
    QSortFilterProxyModel *proxyModel;
    QTreeView *treeAirports;
    QButtonGroup *buttonGroupFilter;
    QLineEdit *txtAirportsFilter;
    QStatusBar *statusBarAirports;
    QLabel *labelAirportsFolder;
    QToolButton *buttonOpenAirportsFolder;
    QTreeWidget *treeAptInfo;
    QStatusBar *statusBarAirportInfo;

    void initialize();
    void load_airports_tree();
    int load_airports_file(QString);

    void load_info_tree(QString airport_dir, QString airport_code);
    void load_tower_node(QString airport_dir, QString airport_code);
    int load_runways_node(QString airport_dir, QString airport_code);
    int load_parking_node(QString airport_dir, QString airport_code);

    QString current_airport();
    QString validate();


public slots:
    void on_airport_tree_selected(QModelIndex, QModelIndex);
    void on_map_double_clicked(QString lat, QString lon, QString heading);
    void on_loadaptdat_done();
    void on_update_airports_filter();
    void on_coordinates_changed();
    void on_airport_info_selection_changed();
    void on_airport_info_double_clicked(QTreeWidgetItem *item, int col_idx);
    void on_open_airports_folder();
    void on_show_metar();


};

#endif // AIRPORTSWIDGET_H
