// aircraftWidget.cpp

#include "app_config.h"

#ifdef ADD_TAB_WIDGET  // make the center a TAB widget

#include "aircraftWidget.h"
#include "mainwindow.h"
#include "loadAircraft.h"

class XTreeWidgetItem : public QTreeWidgetItem
{
public:
    XTreeWidgetItem( QTreeWidget *tree) : QTreeWidgetItem(tree)  {}
    XTreeWidgetItem( QTreeWidgetItem *item) : QTreeWidgetItem(item)  {}
    XTreeWidgetItem( QTreeWidget * parent, const QStringList & strings) : QTreeWidgetItem (parent,strings)  {}

    bool operator< (const QTreeWidgetItem &other) const
    {
        int sortCol = treeWidget()->sortColumn();
        return text(sortCol).toUpper() < other.text(sortCol).toUpper();
    }
};


AircraftWidget::AircraftWidget(QObject *parent)
{
    main = (MainWindow *)parent;
    //* Main Layout
    QVBoxLayout *mainLayout = new QVBoxLayout();
    setLayout(mainLayout);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0,0,0,0);

    //===============================================================
    //= Aircraft Tree
    treeWidget = new QTreeWidget(this);
    //treeLayout->addWidget(treeWidget);
    mainLayout->addWidget(treeWidget);

    treeWidget->setAlternatingRowColors(true);
    treeWidget->setRootIsDecorated(true);
    treeWidget->setUniformRowHeights(true);
    treeWidget->setSortingEnabled(true);
    treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

    treeWidget->headerItem()->setText(C_DIR, "Dir");
    treeWidget->headerItem()->setText(C_RATING, "Rating");
    treeWidget->headerItem()->setText(C_AERO, "Aircraft");
    treeWidget->headerItem()->setText(C_DESCRIPTION, "Description");
    treeWidget->headerItem()->setText(C_FDM, "FDM");
    treeWidget->headerItem()->setText(C_AUTHOR, "Author");
    treeWidget->header()->setStretchLastSection(true);
    treeWidget->setColumnHidden(C_RATING, true);
    treeWidget->setColumnWidth(C_DIR, 60);
    treeWidget->setColumnWidth(C_FDM, 60);
    treeWidget->setColumnWidth(C_DESCRIPTION, 200);

    connect(treeWidget,SIGNAL(itemSelectionChanged()),this,SLOT(on_tree_selection_changed()));

    statusBarTree = new QStatusBar();
    statusBarTree->setSizeGripEnabled(false);
    //treeLayout->addWidget(statusBarTree);
    mainLayout->addWidget(statusBarTree);

    //== Path label
    labelAeroPath = new QLabel();
    statusBarTree->addPermanentWidget(labelAeroPath);

    //== View nested Checkbox
    checkViewNested = new QCheckBox();
    checkViewNested->setText("View folders");
    statusBarTree->addPermanentWidget(checkViewNested);
    connect(checkViewNested, SIGNAL(clicked()), this, SLOT(load_tree()));

    // restore the check
    checkViewNested->setChecked( main->settings->value(S_XVIEW,false).toBool() );

    //== Reload aircrafts
    //QToolButton *actionReloadCacheDb = new QToolButton(this);
    //actionReloadCacheDb->setText("Reload");
    //actionReloadCacheDb->setIcon(QIcon(":/icon/load"));
    //actionReloadCacheDb->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    //actionReloadCacheDb->setAutoRaise(true);
    //statusBarTree->addPermanentWidget(actionReloadCacheDb);
    //connect(actionReloadCacheDb, SIGNAL(clicked()), this, SLOT(on_reload_cache()) );

}

AircraftWidget::~AircraftWidget()
{

}

int AircraftWidget::load_tree_from_list(PAEROLIST pal)
{
    if (!pal)
        return 0;
    int max = pal->count();
    if (!max)
        return 0;
    lockTreeMutex();
    TREE_VIEWS  view = 	(checkViewNested->isChecked()) ? FOLDER_VIEW : LIST_VIEW;
    QString aero = selected_aircraft();
    if (aero.isEmpty()) { // ok, see if there is a 'saved' selection
        aero = main->settings->value(S_AERO,"").toString();
    }
    int cnt;

    treeWidget->setUpdatesEnabled(false);   // disable updates while filling

    cnt = treeWidget->model()->rowCount();
    treeWidget->model()->removeRows(0, cnt);

    QTreeWidgetItem *parentItem = new QTreeWidgetItem();

    treeWidget->setColumnHidden(C_DIR, view == LIST_VIEW);
    treeWidget->setRootIsDecorated(view == FOLDER_VIEW);

    QString dir;
    QString last_dir("");
    for (cnt = 0; cnt < max; cnt++) {
        PAEROITEM pai = pal->at(cnt);
        dir = pai->directory;
        if ( view == LIST_VIEW ) {
            parentItem = treeWidget->invisibleRootItem();
        } else if ( last_dir != dir ) { // cols.at(C_DIR)){
            parentItem = new XTreeWidgetItem(treeWidget->invisibleRootItem());
            parentItem->setText( C_DIR, dir );  // cols.at(C_DIR));
            //parentItem->setIcon(C_DIR, QIcon(":/icon/folder"));
            treeWidget->addTopLevelItem(parentItem);
            treeWidget->setFirstItemColumnSpanned(parentItem, true);
            last_dir = dir; // cols.at(C_DIR);
        }

        XTreeWidgetItem *aeroItem = new XTreeWidgetItem(parentItem);
        //QString xml_path = QString("%1/%2").arg(cols.at(C_DIR)).arg(C_XML_SET);
        //aeroItem->setText(C_XML_SET, xml_path);
        aeroItem->setText(C_AERO, pai->aero);    // cols.at(C_AERO));
        //aeroItem->setIcon(C_AERO, QIcon(":/icon/aircraft"));
        aeroItem->setText(C_DESCRIPTION, pai->description); // cols.at(C_DESCRIPTION));
        aeroItem->setText(C_FDM, pai->fdm);  // cols.at(C_FDM));
        aeroItem->setText(C_AUTHOR, pai->author);   // cols.at(C_AUTHOR));
    }

    treeWidget->sortByColumn(view == FOLDER_VIEW ? C_DIR : C_AERO, Qt::AscendingOrder);
    treeWidget->setUpdatesEnabled(true);

    if ( select_node(aero) ) {
        on_tree_selection_changed();
    }
    outLog("AircraftWidget::load_tree_from_list done");
    unlockTreeMutex();
    return cnt;   // return COUNT
}

//=============================================================
// Load Aircaft To Tree
int AircraftWidget::load_tree()
{
    lockTreeMutex();
    TREE_VIEWS  view = 	(checkViewNested->isChecked()) ? FOLDER_VIEW : LIST_VIEW;
    QString aero = selected_aircraft();
    if (aero.isEmpty()) { // ok, see if there is a 'saved' selection
        aero = main->settings->value(S_AERO,"").toString();
    }
    int c = 0;

    treeWidget->setUpdatesEnabled(false);   // disable updates while filling

    // oops, it looks like this did NOT remove the 'rows'???
    // so checkit - help says only works if the model() supports this
    c = treeWidget->model()->rowCount();
    treeWidget->model()->removeRows(0, c);
    c = treeWidget->model()->rowCount();

    QTreeWidgetItem *parentItem = new QTreeWidgetItem();

    treeWidget->setColumnHidden(C_DIR, view == LIST_VIEW);
    treeWidget->setRootIsDecorated(view == FOLDER_VIEW);

    c = 0;  // restart counter
    // fill the tree
    if (main->aeroLoad) {
        QString dir;
        QString last_dir("");
        PAEROITEM pai = main->aeroLoad->getFirstAero();
        if (pai) {
            do {
                dir = pai->directory;
                if ( view == LIST_VIEW ) {
                    parentItem = treeWidget->invisibleRootItem();
                } else if ( last_dir != dir ) { // cols.at(C_DIR)){
                    parentItem = new XTreeWidgetItem(treeWidget->invisibleRootItem());
                    parentItem->setText( C_DIR, dir );  // cols.at(C_DIR));
                    //parentItem->setIcon(C_DIR, QIcon(":/icon/folder"));
                    treeWidget->addTopLevelItem(parentItem);
                    treeWidget->setFirstItemColumnSpanned(parentItem, true);
                    last_dir = dir; // cols.at(C_DIR);
                }

                XTreeWidgetItem *aeroItem = new XTreeWidgetItem(parentItem);
                //QString xml_path = QString("%1/%2").arg(cols.at(C_DIR)).arg(C_XML_SET);
                //aeroItem->setText(C_XML_SET, xml_path);
                aeroItem->setText(C_AERO, pai->aero);    // cols.at(C_AERO));
                //aeroItem->setIcon(C_AERO, QIcon(":/icon/aircraft"));
                aeroItem->setText(C_DESCRIPTION, pai->description); // cols.at(C_DESCRIPTION));
                aeroItem->setText(C_FDM, pai->fdm);  // cols.at(C_FDM));
                aeroItem->setText(C_AUTHOR, pai->author);   // cols.at(C_AUTHOR));
                c++;
            } while ((pai = main->aeroLoad->getNextAero()) != 0);
        }

    }

    treeWidget->sortByColumn(view == FOLDER_VIEW ? C_DIR : C_AERO, Qt::AscendingOrder);
    treeWidget->setUpdatesEnabled(true);

    // add message
    QString str = QString("%1 aircraft(s)").arg(c);
    statusBarTree->showMessage(str);

    if ( select_node(aero) ) {
        str.append(" - selected "+aero);
    } else {
        if (aero.length())
            str.append(" - select ["+aero+"] failed?");
        else
            str.append(" - no selected aero!");
    }
    outLog("*** FGx: AircraftWidget::load_tree: with " + str);
    on_tree_selection_changed();
    unlockTreeMutex();
    return c;   // return COUNT
}

//==============================
//== Select an aircraft in tree
bool AircraftWidget::select_node(QString aero)
{
    if (aero.length()) {
        QList<QTreeWidgetItem*> items = treeWidget->findItems(aero, Qt::MatchExactly | Qt::MatchRecursive, C_AERO);
        if ( items.length() > 0 ) {
            treeWidget->setCurrentItem(items[0]);
            treeWidget->scrollToItem(items[0], QAbstractItemView::PositionAtCenter);
            return true;
        }
    }
    return false;
}
//==============================
//== return selected Aircraft
QString AircraftWidget::selected_aircraft()
{
    QTreeWidgetItem *item = treeWidget->currentItem();
    if( !item || (item->text(C_AERO).length() == 0) ) {
        return "";
    }
    return item->text(C_AERO);
}

//==========================================================================1
// Aircraft Selected
//==========================================================================
void AircraftWidget::on_tree_selection_changed()
{
    QTreeWidgetItem *item = treeWidget->currentItem();
    if ( !item ) {
        outLog("on_tree_selection_changed: no selected item");
        labelAeroPath->setText("No selected aircraft!");
        //buttonAeroPath->setDisabled(true);
        return;
    }

    //= Check there is item and its a model
    QString aero = item->text(C_AERO);
    if ( aero.length() == 0 ) {
        outLog("on_tree_selection_changed: no C_AERO item");
        //aeroImageLabel->clear();
        //emit setx("--aircraft=", false, "");
        labelAeroPath->setText("aero not valid on selection!");
        //buttonAeroPath->setDisabled(true);
        return;
    }

    //qDebug() << item->text(C_DIR);
    // labelAeroPath->setText(mainObject->X->aircraft_path() + "/" + item->text(C_AERO));
    QString path = main->get_aircraft_path();
    path.append("/" + aero);
    labelAeroPath->setText(path);
    outLog("on_tree_selection_changed: "+path);
    //buttonAeroPath->setDisabled(false);

    //= Get the thumbnail image
    //QString thumb_file = QString("%1/%2/%3/thumbnail.jpg").arg( mainObject->X->aircraft_path(),
    //																item->text(C_DIR),
    //																item->text(C_AERO));
    //if(QFile::exists(thumb_file))/{
    //	QPixmap aeroImage(thumb_file);
    //	if(!aeroImage.isNull()){
    //		outLog("on_tree_selection_changed: Loaded thumb "+thumb_file);
    //		aeroImageLabel->setText("");
    //		aeroImageLabel->setPixmap(aeroImage);
    //} else {
        //outLog("on_tree_selection_changed: NO thumb load "+thumb_file);
        //	aeroImageLabel->clear();
        //	aeroImageLabel->setText("No Image");
    //}
    //}else{
    //	aeroImageLabel->setText("No Image");
    //}
    //emit setx("--aircraft=", true, selected_aircraft());
    main->settings->setValue(S_AERO, aero);
}

#endif // #ifdef ADD_TAB_WIDGET  // make the center a TAB widget
// eof - aircraftWidget.cpp


