/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Sep 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */
#ifndef APP_CONFIG_H
#define APP_CONFIG_H

#define APP_NAME "FGx-test"
#define APP_VERS "0.0.2"
#define APP_DATE "2012-02-23"

#define APP_LOG  "fgx-test.txt";

// try the bing map layers
// #define USE_BING_LAYERS // FAILED???

// default locations
#define USE_DEFAULT_KSFO    // since added multiple destinations
// and later added apt.dat load, and list expanded to about 26,000 airports
// ***TBD*** But thus need some way to filter list, or keep set of ok points
// =========================================================================
#define KSFO_LAT    "37.618763"
#define KSFO_LON    "-122.374926"
#define KSFO_ZOOM   14

#define YGIL_LAT    "-31.699"
#define YGIL_LON    "148.635"
#define YGIL_ZOOM   12

// rather than the above miserly list, added a set of easy to
// select locations in a QListWidget ;=))
#define ADD_LIST_WIDGET
#define USE_ALLOC_DIALOG
#define ADD_TAB_WIDGET  // make the center a TAB widget
#define ADD_AIRPORT_WIDGET  // add airport + info lists

// Settings use INI file stored in
// QDir(QDesktopServices::storageLocation(QDesktopServices::DataLocation)).absolutePath();
// That is ~/.local/share/data/geoffair/Qt_OSM_Map/default.ini in Ubuntu
#define USE_INI_FILE
#define DEF_INI_FILE    "default.ini"
#define AIRCRAFT_CACHE  "aircraft2.txt"
#define AIRPORT_CACHE   "airport2.txt"
#define ENABLE_NEW_AIRPORT_PATH // build path from ICAO only, plus current scenery path(s)

#define DEF_TIMEOUT 10000   // ten seconds for status messages

// Thread - User types
#define tht_loadAptDat  1
#define tht_loadAptXml  2

// Keys for Settings
// change these ONLY for a good reason
// ***********************************
#define APP_SETV "0.0.1"
#define APP_SETN "FGx-test"

#define S_MAPLON    "map/lon"
#define S_MAPLAT    "map/lat"
#define S_MAPZOOM   "map/zoom"

#define S_FILENAME "previous/filename"
#define S_DIRNAME "previous/directory"

#define S_ROOT "current/fgroot"
#define S_SCENE "current/scenery"
#define S_FGAPTDAT "current/fgaptdat"
#define S_AIRPATH "current/airpath"

#define S_LISTROOT "lists/fgroot"
#define S_LISTSCENE "lists/scenery"
#define S_LISTFGAPTDAT "lists/fgaptdat"
#define S_LISTICAO "lists/icao"

#define S_NATDIR "native/directory"
#define S_NATOPTS "native/options"
#define S_NATFILE "native/filename"

#define S_ICAO  "current/icao"
#define S_COUNT "current/count"
#define S_AERO  "current/aero"

#define S_XHELI "check/xheli"
#define S_XSEA "check/xsea"
#define S_XCLOSED "check/xclosed"
#define S_XVIEW  "check/xview"

#define S_DETRUN "check/runways"
#define S_DETCOMM "check/comms"

#define S_TABIDX "tab/index"
// *************************************

enum TREE_COLS {
    C_DIR = 0,
    C_AERO,
    C_RATING,
    C_DESCRIPTION,
    C_FDM,
    C_AUTHOR,
    C_FILE_PATH,
    // last
    C_MAX_COLS
};


#endif // APP_CONFIG_H
