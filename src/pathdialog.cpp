/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   ================================================== */

#include "app_config.h"
#include "pathdialog.h"
#include "dirDialog.h"
#include "mainwindow.h"
#include "utilities/utilities.h"

pathDialog::pathDialog(QWidget *parent) :
    QDialog(parent)
{
    parent_not_used = parent;
    setWindowTitle("FG Root and Scenery Paths");
    setMinimumWidth(600);

    root_valid = false;
    scene_valid = false;
    exit_ok = false;

    int m = 5;
    int row = 0;
    int cols = 8;

    mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(m,m,m,m);
    mainLayout->setSpacing(10);
    setLayout(mainLayout);

    info = new QLabel("Get Flightgear root and scenery paths",this);
    mainLayout->addWidget(info,row,0,1,cols);
    row++;

    // FG_ROOT Path
    // ===================================================
    // UI info and input
    rootLab = new QLabel("FG_ROOT:",this);
    rootEd = new QLineEdit(this);
    rootBut = new QPushButton("...",this);
    rootLab->setAlignment(Qt::AlignRight);
    rootList = new QListWidget(this);
    rootList->setSelectionMode(QAbstractItemView::SingleSelection);
    rootList->setSelectionBehavior(QAbstractItemView::SelectRows);

    // placement
    mainLayout->addWidget(rootLab,row,0,1,1);
    mainLayout->addWidget(rootEd,row,1,1,cols-2);
    mainLayout->addWidget(rootBut,row,cols-1,1,1);
    row++;
    mainLayout->addWidget(rootList,row,1,3,cols-2);
    row += 3;

    // connections
    connect(rootEd,SIGNAL(textChanged(QString)),this,SLOT(on_root_changed(QString)));
    connect(rootBut,SIGNAL(clicked()),this,SLOT(on_browse_root()));
    connect(rootList,SIGNAL(itemSelectionChanged()),this,SLOT(on_root_selection()));
    // ===================================================

    // FG_SCENERY Path
    // ===================================================
    // UI info and input
    sceneLab = new QLabel("Scenery:",this);
    sceneEd = new QLineEdit(this);
    sceneBut = new QPushButton("...",this);
    sceneLab->setAlignment(Qt::AlignRight);
    sceneList = new QListWidget(this);
    sceneList->setSelectionMode(QAbstractItemView::SingleSelection);

    // placement
    mainLayout->addWidget(sceneLab,row,0,1,1);
    mainLayout->addWidget(sceneEd,row,1,1,cols-2);
    mainLayout->addWidget(sceneBut,row,cols-1,1,1);
    row++;
    sceneList->setSelectionBehavior(QAbstractItemView::SelectRows);
    mainLayout->addWidget(sceneList,row,1,3,cols-2);
    row += 3;

    // connections
    connect(sceneEd,SIGNAL(textChanged(QString)),this,SLOT(on_scene_changed(QString)));
    connect(sceneBut,SIGNAL(clicked()),this,SLOT(on_browse_scene()));
    connect(sceneList,SIGNAL(itemSelectionChanged()),this,SLOT(on_scene_selection()));
    // ===================================================


    // cancel and OK buttons
    // allocation
    cancelBut = new QPushButton("Cancel",this);
    okBut = new QPushButton("Ok",this);
    // placement
    mainLayout->addWidget(cancelBut,row,cols-2);
    mainLayout->addWidget(okBut,row,cols-1);
    // connections
    connect(cancelBut,SIGNAL(clicked()),this,SLOT(on_cancel()));
    connect(okBut,SIGNAL(clicked()),this,SLOT(on_ok()));

}

/* ----------------------------------------------------
   User intialisation of the pathDialog
   ---------------------------------------------------- */
void pathDialog::init(QString root, QString scene, QStringList lroot, QStringList lscene)
{
    int i, cnt, rows, j;
    int fh = 16;    // gestimate font height (pixels)
    int mrows = 5;  // maximim rows
    int rowadj = 2; // an adjustment, for the boder region
    QString tmp;

    QFont font = rootList->font();
    QFontMetrics met(font);
    fh = met.height();
    if (fh < 16)
        fh = 16;
    j = rootList->sizeHintForRow(0);
    if (j > fh)
        fh = j;

    // add what user gave
    rootEd->setText(root);
    sceneEd->setText(scene);

    // deal with any root list given
    rows = 0;
    rootList->clear();
    if (root.length()) {
        new QListWidgetItem(root,rootList);
        rootList->item(0)->setSelected(true);
        j = rootList->sizeHintForRow(rows);
        if (j > fh)
            fh = j;
        rows++;
    }
    cnt = lroot.count();
    for (i = 0; i < cnt; i++) {
        // add to list
        tmp = lroot.at(i);
        if (tmp.length() && (tmp != root)) {
            new QListWidgetItem(tmp,rootList);
            j = rootList->sizeHintForRow(rows);
            if (j > fh)
                fh = j;
            rows++;
        }
    }

    // moderate rows value
    if (rows > mrows)
        rows = mrows;
    else if (rows < 2)
        rows = 2;
    else
        rows++; // plus one blank
    rootList->setMaximumHeight((rows*fh)+(rows*rowadj));

    // deal with any scenery list given
    rows = 0;
    sceneList->clear();
    if (scene.length()) {
        new QListWidgetItem(scene,sceneList);
        sceneList->item(0)->setSelected(true);
        j = rootList->sizeHintForRow(rows);
        if (j > fh)
            fh = j;
        rows++;
    }
    cnt = lscene.count();
    for (i = 0; i < cnt; i++) {
        // add to list
        tmp = lscene.at(i);
        if (tmp.length() && (tmp != scene)) {
            new QListWidgetItem(tmp,sceneList);
            j = rootList->sizeHintForRow(rows);
            if (j > fh)
                fh = j;
            rows++;
        }
    }

    // moderate rows value
    if (rows > mrows)
        rows = mrows;
    else if (rows < 2)
        rows = 2;
    else
        rows++; // add one blank row
    sceneList->setMaximumHeight((rows*fh)+(rows*rowadj));

    // set green or red on labels
    on_root_changed(root);
    on_scene_changed(scene);
}

void pathDialog::on_cancel()
{
    close();
}

void pathDialog::on_ok()
{
    exit_ok = true;
    emit on_user_ok(rootEd->text(),sceneEd->text());
    close();
}

bool pathDialog::is_valid_fg_root(QString txt)
{
    return  util_isValidFGRootDir(txt);
}

bool pathDialog::is_valid_fg_scenery(QString txt)
{
    return util_isValidFGSceneryDir(txt);
}

void pathDialog::on_browse_root()
{
    dirDialog d(this);
    d.init("Get FG Data Directory",rootEd->text());
    d.exec();
    QString path = d.getDirName();
    if (path.length()) {
        if (is_valid_fg_root(path)) {
            rootEd->setText(path);
            on_root_changed(path);
            QString scene = sceneEd->text();
            if (scene.length() == 0) {
                scene = path;
                scene.append("/Scenery");
                if (is_valid_fg_scenery(scene)) {
                    sceneEd->setText(scene);
                    on_scene_changed(scene);
                }
            }
        }
    }
}

void pathDialog::on_browse_scene()
{
    dirDialog d;
    d.init("Get Scenery Directory",sceneEd->text());
    d.exec();
    QString path = d.getDirName();
    if (path.length()) {
        sceneEd->setText(path);
        on_scene_changed(path);
    }
}

void pathDialog::on_root_changed(QString txt)
{
    QString style("color:red");
    root_valid = false;
    if (txt.length()) {
        QString tmp(txt);
        if (util_ensureUnixPathSep(tmp)) {
            rootEd->setText(tmp);
        }
        if (is_valid_fg_root(tmp)) {
            style = "color:green";
            root_valid = true;
        }
    }
    rootLab->setStyleSheet(style);
}

void pathDialog::on_scene_changed(QString txt)
{
    QString style("color:red");
    scene_valid = false;
    if (txt.length()) {
        QString tmp(txt);
        if (util_ensureUnixPathSep(tmp))
            sceneEd->setText(tmp);
        if (is_valid_fg_scenery(tmp)) {
            style = "color:green";
            scene_valid = true;
        }
    }
    sceneLab->setStyleSheet(style);
}

void pathDialog::on_root_selection()
{
    QListWidgetItem *item = rootList->currentItem();
    if (!item)
        return;
    QString path = item->text();
    if (path.length()) {
        rootEd->setText(path);
        on_root_changed(path);
    }
}

void pathDialog::on_scene_selection()
{
    QListWidgetItem *item = sceneList->currentItem();
    if (!item)
        return;
    QString path = item->text();
    if (path.length()) {
        sceneEd->setText(path);
        on_scene_changed(path);
    }
}

// eof - pathdialog.cpp
