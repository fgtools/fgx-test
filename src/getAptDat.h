/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Dec 2011
   License: GPL2 (or later)
   ================================================== */
#ifndef GETAPTDAT_H
#define GETAPTDAT_H
#include "app_config.h"
#include <QDialog>
#include <QString>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QFile>
#include <QListWidget>
#include <QStringList>

class MainWindow;

class getAptDat : public QDialog
{
    Q_OBJECT
public:
    explicit getAptDat(QWidget *parent = 0);
    MainWindow *main;
    bool got_ok;
    void setup(QString prev, QStringList list);
    QString getFile() { return aptdatEd->text(); }

signals:

public slots:
    void on_ok();
    void on_cancel();
    void on_get_aptdat();
    void on_aptdat_selection();
    void on_aptdat_changed(QString);

private:
    QGridLayout *mainLayout;    // = new QGridLayout(this);
    QLabel *infoLab;            // = new QLabel("Enter or search for data files",this);

    QLabel *aptdatLab;          // = new QLabel("FG Apt.dat:",this);
    QLineEdit *aptdatEd;        // = new QLineEdit(this);
    QPushButton *aptdatBut;     // = new QPushButton("...",this);
    QListWidget *aptdatList;    // = new QListWidget(this);

    QPushButton *okBut;         // = new QPushButton("Ok",this);
    QPushButton *cancelBut;     // = new QPushButton("Cancel", this);

};

#endif // GETAPTDAT_H
// eof - getAptDat.h
