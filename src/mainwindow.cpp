/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Sep 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */

#include "app_config.h"
#include <QApplication>
#include "mainwindow.h"
#include "osm_map.h"
#ifdef ADD_TAB_WIDGET
#include "aircraftWidget.h"
#ifdef ADD_AIRPORT_WIDGET
#include "aircraftWidget.h"
#endif // #ifdef ADD_AIRPORT_WIDGET
#endif // #ifdef ADD_TAB_WIDGET
#include "testdialog.h"
#include "moveDialog.h"
#include "pathdialog.h"
#include "searchDialog.h"
#include "getAptDat.h"
#include "utilities/utilities.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QString tmp;
    in_setup = true;    // AVOID certain actions, while in setup...
    m_time.start(); // start a time counter
    debug_mode = false;
    airdata = 0;
    done_init = false;
    test7_thread_done = false;
    done_init6 = false;
    test6_thread_done = false;
    aeroLoad = 0;
    test10_thread_done = false;

    // time out each second will handle this, all on a thread
    auto_load_aptdat = true;
    auto_load_xml = true;
    auto_load_aircraft = true;


#ifdef USE_INI_FILE
    tmp = get_data_file(DEF_INI_FILE);
    settings = new QSettings(tmp,QSettings::IniFormat,this);
    tmp = "MainWindow: Using INI file ["+tmp+"]";
#else // !USE_INI_FILE
    settings = new QSettings(this);
    tmp = "MainWindow: Using 'generic' global settings";
#endif // USE_INI_FILE y/b

    util_setStdLogFile();   // create 'standard' LOG file
    outLog(tmp);            // advise type of settings used

    // restore windows size and postion - saved at closeEvent(QCloseEvent*)
    restoreGeometry(settings->value("mainWindowGeometry").toByteArray());

#ifdef LOADAIRPORTS_H
    loadair = 0;
#endif // #ifdef LOADAIRPORTS_H
    widget = new QWidget(this);
    setCentralWidget(widget);

    int m = 10;
    mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(m,m,m,m);
    mainLayout->setSpacing(0);
    widget->setLayout(mainLayout);
    //centralWidget()->setLayout(mainLayout);

    menuBar = new QMenuBar(this);

    mainLayout->addWidget(menuBar);

    menuFile = new QMenu(tr("&File"),this);
    exitAct = menuFile->addAction(tr("&Quit"),this,SLOT(on_exit()));
    //connect(exitAct,SIGNAL(triggered()),this,SLOT(on_exit()));
    menuBar->addMenu(menuFile);

    menuTest = new QMenu(tr("&Test"),this);
    testAct5 = menuTest->addAction(tr("Set Root &Paths..."),this,SLOT(on_test5())); // &P
    testAct9 = menuTest->addAction(tr("Set D&ata files..."),this,SLOT(on_test9())); // &A
    testAct = menuTest->addAction(tr("&Move Map..."),this,SLOT(on_test1()));        // &M
    testAct2 = menuTest->addAction(tr("Get &File..."),this,SLOT(on_test2()));       // &F
    testAct3 = menuTest->addAction(tr("Get &Dir..."),this,SLOT(on_test3()));        // &D
    testAct4 = menuTest->addAction(tr("&Native Gets..."),this,SLOT(on_test4()));    // &N
    testAct6 = menuTest->addAction(tr("Load &Airports XML"),this,SLOT(on_test6_thread())); // &A
    testAct7 = menuTest->addAction(tr("&Load Apt.dat"),this,SLOT(on_test7_thread()));      // &L
    testAct8 = menuTest->addAction(tr("&Search ICAO..."),this,SLOT(on_test8()));    // &S
    testAct10 = menuTest->addAction(tr("Load aircraft (thread)"),this,SLOT(on_test10()));
    testAct11 = menuTest->addAction(tr("Load aircraft cache"),this,SLOT(on_test11()));
    testAct100 = menuTest->addAction(tr("Mark log"),this,SLOT(on_test100()));
    menuBar->addMenu(menuTest);

    menuHelp = new QMenu(tr("&Help"),this);
    helpAct = menuHelp->addAction(tr("&About"),this,SLOT(on_about()));
    abtqtAct = menuHelp->addAction(tr("&About Qt"),this,SLOT(on_about_qt()));
    menuBar->addMenu(menuHelp);


    osmmap = new osmMap(this);
    osmmap->load_map("airport");
    // osmmap->zoom_to_airport("YGIL"); - this FAILS
    // and this??? osmmap->zoom_to_latlon("-31.699", "148.635", 12);

#ifdef ADD_TAB_WIDGET
    tabWidget = new QTabWidget(this);
    //tabWidget->setObjectName("launcher_tabs");
    mainLayout->addWidget(tabWidget);
    connect(tabWidget, SIGNAL(currentChanged(int)), this, SLOT(on_tab_changed(int)));
    //* Map
    tabWidget->addTab(osmmap, tr("Map"));

    //* Aircraft
    aeroWidget = new AircraftWidget(this);
    tabWidget->addTab(aeroWidget, tr("Aircraft"));
#ifdef ADD_AIRPORT_WIDGET
    //*airports
    airportWidget = new AirportsWidget(this);
    tabWidget->addTab(airportWidget, tr("Airport"));
#endif // #ifdef ADD_AIRPORT_WIDGET

#else // !ADD_TAB_WIDGET
    mainLayout->addWidget(osmmap);
#endif // ADD_TAB_WIDGET y/n

    // == A test results group
    resultsLayout = new QVBoxLayout(this);
    resultsLayout->setContentsMargins(10, 2, 10, 2);
    resultsLayout->setSpacing(2);

    tmp = settings->value(S_FILENAME, "NewProfile.ini").toString();
    infoLabel = new QLabel("FILE: "+tmp,this);

    tmp = settings->value(S_DIRNAME, util_getCurrentWorkDirectory()).toString();
    infoLabel2 = new QLabel("DIR: "+tmp,this);

    tmp = settings->value(S_FGAPTDAT,"").toString();
    fg_apt_dat_file = tmp;
    infoLabel7 = new QLabel("FG Apt.dat: "+tmp,this);

    current_root = settings->value(S_ROOT, "").toString();
    infoLabel3 = new QLabel("FG_ROOT: "+current_root,this);

    current_scenery = settings->value(S_SCENE, "").toString();
    infoLabel4 = new QLabel("Scenery: "+current_scenery,this);
    infoLabel5 = new QLabel("Load airport xml: ",this);
    infoLabel6 = new QLabel("Load Apt.dat: ",this);

    root_list = settings->value(S_LISTROOT,"").toStringList();
    scene_list = settings->value(S_LISTSCENE,"").toStringList();
    aptdat_list = settings->value(S_LISTFGAPTDAT,"").toStringList();
    saveAptDatList(fg_apt_dat_file);    // ensure current is in list

    resultsLayout->addWidget(infoLabel);    // add widgets
    resultsLayout->addWidget(infoLabel2);
    resultsLayout->addWidget(infoLabel3);
    resultsLayout->addWidget(infoLabel4);
    resultsLayout->addWidget(infoLabel7);
    resultsLayout->addWidget(infoLabel5);
    resultsLayout->addWidget(infoLabel6);

#ifdef ADD_TAB_WIDGET
    infoLabel8 = new QLabel("Load aircraft xml: ",this);
    resultsLayout->addWidget(infoLabel8);
    infoLabel9 = new QLabel("aircraft cache: ",this);
    resultsLayout->addWidget(infoLabel9);

    resultsWidget = new QWidget(this);
    resultsWidget->setLayout(resultsLayout);
    tabWidget->addTab(resultsWidget, tr("Results"));


    resultsEdit = new QPlainTextEdit(this);
    resultsEdit->setReadOnly(true);
    resultsLayout->addWidget(resultsEdit);
    //= retore last tab
    // -----------------------------------------------
    // settings->setValue(S_TABIDX,tab_idx);
    int tab_idx = settings->value(S_TABIDX,-1).toInt();
    tmp.sprintf("Last tab index %d ",tab_idx);
    if ((tab_idx >= 0)&&(tab_idx < T_MAX_VAL)) {
        tabWidget->setCurrentIndex(tab_idx);
        tmp.append("restored");
    } else {
        tmp.append("OUT OF RANGE???");
    }
    outLog(tmp);
    // -----------------------------------------------

#else // !ADD_TAB_WIDGET
    resultsGroup = new QGroupBox(tr("Test Results"),this);
    resultsGroup->setLayout(resultsLayout); // set vertical layout
    mainLayout->addWidget(resultsGroup);    // add to window
#endif // ADD_TAB_WIDGET y/n


    // add status bar at bottom
    // ************************
    statusBar = new QStatusBar(this);
    labelTime = new QLabel(tr("00:00:00"),this);
    labelTime->setFrameStyle(QFrame::Panel | QFrame::Raised);
    statusBar->addPermanentWidget(labelTime);
    connect(statusBar,SIGNAL(messageChanged(QString)),this,SLOT(on_message_changed(QString)));
    mainLayout->addWidget(statusBar);

    done_setmap = false;

    testdialog = new testDialog(this);

#ifdef USE_ALLOC_DIALOG
    movedialog = new moveDialog(this);
    connect(movedialog,SIGNAL(set_position(QString,QString,int)),this,SLOT(move_map_position(QString,QString,int)));
#endif // #ifdef USE_ALLOC_DIALOG

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(on_time_out()));

    tmp = "MainWindow: reset in_setup, in ";
    tmp.append(getElapTimeStg(m_time.elapsed()));
    outLog(tmp);
    in_setup = false; // AVOID certain actions, while in setup...
    load_aircraft_cache();
    load_airport_cache();
    tmp = "MainWindow: exit setup in ";
    tmp.append(getElapTimeStg(m_time.elapsed()));
    outLog(tmp);
    // start timer tick
    timer->start(1000); // LAST ACTION: start timer tick
}

MainWindow::~MainWindow()
{
#ifdef LOADAIRPORTS_H
    if (loadair)
        delete loadair;
#endif // #ifdef LOADAIRPORTS_H
#ifdef LOADAPTDAT_H
    if (airdata) {
        airdata->clear_list();
        delete airdata;
    }
#endif // #ifdef LOADAPTDAT_H
    if (aeroLoad)
        delete aeroLoad;

}

void MainWindow::load_airport_cache()
{
#ifdef ADD_TAB_WIDGET
#ifdef ADD_AIRPORT_WIDGET
    QTime tm;
    QString file1;
    file1 = get_data_file(AIRPORT_CACHE);
    QFile file(file1);
    if (!file.exists()) {
        outLog("File "+file1+" does NOT exist! No load done");
        return;
    }
    //*airports
    //airportWidget = new AirportsWidget(this);
    tm.start();
    int cnt = airportWidget->load_airports_file(file1);
    QString msg;
    msg.sprintf("Loaded %d airports,",cnt);
    msg.append(" from "+file1+", in ");
    msg.append(getElapTimeStg(tm.elapsed()));
    outLog(msg);
#endif
#endif
}


void MainWindow::on_exit()
{
    close();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    settings->setValue("mainWindowGeometry", saveGeometry());
    outLog(util_getDateTimestg()+" - Application close",0x8001);
    event->accept();

}

void MainWindow::on_about()
{
    QString msg;
    msg = "Version: ";
    msg.append(APP_VERS);
    msg.append(", dated ");
    msg.append(APP_DATE);
    msg.append("\n");
    msg.append("Built: ");
    msg.append(__DATE__);
    msg.append(" at ");
    msg.append(__TIME__);
    msg.append("\n\n");
    msg.append("Qt Test Gui shows various aspects of Qt, and\n");
    msg.append("to load and show an OpenStreetMap map\n");
    msg.append("License: GPL v2 or later\n\n");
    // msg.append("Source: http://geoffair.org/projects/qt_osm_map.htm");
    msg.append("The session log file is [");
    msg.append(util_getLogFile());
    msg.append("]\n");
    QMessageBox::about(this, tr("About Qt OSM Map"), msg);
}

void MainWindow::on_about_qt()
{
    QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::move_map_position(QString lat, QString lon, int zoom)
{
    QString msg;
    msg.sprintf("%d",zoom);
    msg = "move_map_postion: "+lat+", "+lon+", "+msg;
    osmmap->zoom_to_latlon(lat, lon, zoom);
    osmmap->map_set_coords(lat,lon);
    settings->setValue(S_MAPLAT,lat);
    settings->setValue(S_MAPLON,lon);
    settings->setValue(S_MAPZOOM,zoom);
    setStatusMessage(msg,DEF_TIMEOUT);
}

void MainWindow::on_time_out()
{
    QString msg = util_getTimestg();
    bool to = false;
    if (airdata && airdata->isThreadInFunction())
        to = true;
    else if (loadair && loadair->isThreadInFunction())
        to = true;
    else if (aeroLoad && aeroLoad->isThreadInFunction())
        to = true;
    if (to)
        msg.append("*");
    labelTime->setText(msg);

    // any events scheduled
    if (!done_setmap && !in_setup) {
        //int ms = m_time.elapsed();
        //if (ms >= 1000) {
            done_setmap = true;
            QString slat = settings->value(S_MAPLAT,KSFO_LAT).toString();
            QString slon = settings->value(S_MAPLON,KSFO_LON).toString();
            int zoom = settings->value(S_MAPZOOM,KSFO_ZOOM).toInt();
            osmmap->zoom_to_latlon(slat, slon, zoom);
            osmmap->map_set_coords(slat,slon);
        //}
    }

    if (done_init) {
        if (test7_thread_done) {
            if (!done_init6) {
                done_init6 = true;
                if (auto_load_xml)
                    on_test6_thread();  // run the xml load, on a thread
            }
        }
    } else {
        done_init = true;
        if (auto_load_aptdat)
            on_test7_thread();  // use thread to LOAD apt.dat, if a valid file
    }
    if (auto_load_aircraft) {
        auto_load_aircraft = false;
        outLog("on_time_out: Doing AUTO on_test10...");
        on_test10();
    }
}

void MainWindow::setStatusMessage(QString msg, int timeout)
{
    statusBar->showMessage(msg,timeout);
#ifdef ADD_TAB_WIDGET
    if (msg != last_status_msg) {
        // do NOT repeat messages
        resultsEdit->appendPlainText(msg);
    }
#endif //
    last_status_msg = msg;
    //QString tmp;
    //tmp.sprintf(" (to=%d secs)", (int)(timeout / 1000));
    outLog("STATUS: "+msg);
}

void MainWindow::on_message_changed(QString msg)
{
    if (msg.isEmpty()) {
        QString tmp("Ready");
        bool to = false;
        if (airdata && airdata->isThreadInFunction()) {
            tmp.append(" - apt.dat thread running");
            to = true;
        }
        if (loadair && loadair->isThreadInFunction()) {
            tmp.append(" - scan xml thread running");
            to = true;
        }
        if (to)
            setStatusMessage(tmp,1000); // make it short lived, like 1 sec
        else
            statusBar->showMessage(tmp);
    }
}

//===========================================================================
//** Data File eg default.ini
//===========================================================================
/** \brief Path to a data file eg get_data_file("default.ini")
 * \return Absolute path to the file
 */
QString MainWindow::get_data_file(QString file_name)
{
    QString storedir = QDir(QDesktopServices::storageLocation(QDesktopServices::DataLocation)).absolutePath();

    // create path is not exist
    if(!QFile::exists(storedir)){
        QDir *dir = new QDir("");
        dir->mkpath(storedir);
    }
    return storedir.append("/").append(file_name);
}



#ifdef USE_ALLOC_DIALOG

void MainWindow::on_test1()
{
    // get a new location
    OSMPOS pos;
    osmmap->get_osm_pos(&pos);  // get OSM map position
    QString msg;
    msg.sprintf("%d",pos.zoom);

    movedialog->finalise(); // repopulate with list

    // set current values in dialog
    movedialog->latEd->setText(pos.lat);
    movedialog->lonEd->setText(pos.lon);
    movedialog->zoomEd->setText(msg);
    movedialog->select_table_row(pos.lat,pos.lon);

    movedialog->exec();
}

#else // !#ifdef USE_ALLOC_DIALOG

void MainWindow::on_test1()
{
    // get a new location
    //QDialog *d = new QDialog(this);
    //moveDialog *d = new moveDialog(this);
    moveDialog movedialog(this);
    OSMPOS pos;
    osmmap->get_osm_pos(&pos);  // get OSM map position
    QString msg;
    msg.sprintf("%d",pos.zoom);

    // set default values in dialog
    movedialog.latEd->setText(pos.lat);
    movedialog.lonEd->setText(pos.lon);
    movedialog.zoomEd->setText(msg);
    movedialog.select_table_row(pos.lat,pos.lon); // does nothing if no #ifdef ADD_LIST_WIDGET

    connect(&movedialog,SIGNAL(set_position(QString,QString,int)),this,SLOT(move_map_position(QString,QString,int)));

    movedialog.exec();
    //d->exec();
    //delete d;
}

#endif // #ifdef USE_ALLOC_DIALOG y/n

void MainWindow::on_test2()
{
    QString title = "Select File Name";
    QString key = S_FILENAME;
    QString prev = settings->value(key, "NewProfile.ini").toString();
    QStringList filt;
    filt += "*.ini";
    QString file = util_getFileName( (QWidget *)this,title, prev, filt);
    QString msg = "Got new file ["+file+"]";
    if (file.length()) {
        settings->setValue(key,file);
        infoLabel->setText("FILE: "+file);
    }
    setStatusMessage(msg,DEF_TIMEOUT);

}

void MainWindow::on_test3()
{
    QString title = "Select Existing Directory";
    QString key = S_DIRNAME;
    QString prev = settings->value(key, "").toString();
    //// prev.append("/");   // JUST FOR TESTING
    QString dir = util_getDirName( (QWidget *)this,title, prev);
    QString msg = "Got new dir ["+dir+"]";
    if (dir.length()) {
        settings->setValue(key,dir);
        infoLabel2->setText("DIR: "+dir);
    }
    setStatusMessage(msg,DEF_TIMEOUT);
}

void MainWindow::on_test4()
{
    //testDialog *d = new testDialog(this);
    testdialog->exec();

}

void MainWindow::on_test5()
{
    QString tmp, msg, root, scene, aero;
    QDir dir;
    outLog("Doing test5 - Get root and scenery paths");
    pathDialog d(this);
    root = settings->value(S_ROOT, "").toString();
    scene = settings->value(S_SCENE, "").toString();
    d.init(root,scene,root_list,scene_list);

    d.exec();
    if (d.exit_ok) {
        //tmp = settings->value(S_ROOT, "").toString();
        tmp = d.get_root();
        msg = "FG_ROOT: "+tmp;
        if (d.root_valid && tmp.length()) {
            if (tmp != root) { // is it a NEW root
                settings->setValue(S_ROOT,tmp);
                aero = tmp+"/Aircraft";
                if (dir.exists(aero)) {
                    // we have a NEW aircraft path
                    settings->setValue(S_AIRPATH,aero);
                    auto_load_aircraft = true; // schedule a RE-LOAD of aircraft
                    outLog("Set a NEW aircraft folder "+aero);
                } else {
                    outLog("FG_ROOT changed but "+aero+" NOT valid!");
                }
            }
            if (!root_list.contains(tmp)) {
                root_list += tmp;
                settings->setValue(S_LISTROOT,root_list);
                msg.append(" (+list)");
            }
            infoLabel3->setText(msg);
            outLog("test5: "+msg);
        }
        //tmp = settings->value(S_SCENE, "").toString();
        tmp = d.get_scene();
        msg = "Scenery: "+tmp;
        if (d.scene_valid && tmp.length()) {
            if (tmp != scene)   // is it a NEW value
                settings->setValue(S_SCENE,tmp);
            if (!scene_list.contains(tmp)) {
                scene_list += tmp;
                settings->setValue(S_LISTSCENE,scene_list);
                msg.append(" (+list)");
            }
            infoLabel4->setText(msg);
            outLog("test5: "+msg);
        }
    }
}

void MainWindow::on_test6_thread()
{
    //QTime tm;
    QString msg;
    //tm.start();
    //int file_count, thresh_count, air_count, run_count;
    //file_count = thresh_count = air_count = run_count = 0;
    QString path = settings->value(S_SCENE,"").toString();   // sceneEd->text();
    if (!util_isValidFGSceneryDir(path)) {
        msg = "on_test6_thread: NOT valid scenery directory ["+path+"]";
        setStatusMessage(msg);
        return;
    }

    if (!loadair)
        loadair = new loadAirports(this);
    PLOADLIST pll = &m_loadList;
    loadair->clear_list(pll);   // clear the LIST
    loadair->def_xr_flag = 0;   // clear this flag
    msg = "Scanning ["+path+"] for ICAO.threshold.xml files... on a thread...";
    outLog(msg);
    infoLabel5->setText(msg);
    setStatusMessage(msg);
    connect(loadair,SIGNAL(on_thread_done()),this,SLOT(on_test6_done()));
    loadair->readOnThread(path,pll);
}

// catches emit on_thread_done() in loadAirports
void MainWindow::on_test6_done()
{
    QString msg;
    PLOADLIST pll = &m_loadList;
    int air_count = loadair->air_count;
    int file_count = loadair->file_count;
    int thresh_count = loadair->thresh_count;
    int run_count = loadair->run_count;
    int time_ms = loadair->time_ms;
    if (air_count == file_count) {
        msg.sprintf("airports XML load:  %d (%d) threshold.xml, %d airports, with %d runways.",
                file_count, thresh_count, air_count, run_count);
    } else {
        msg.sprintf("airports XML load:  %d files, %d threshold.xml, %d airports, with %d runways.",
                file_count, thresh_count, air_count, run_count);
    }

    msg.append(", in "+getElapTimeStg(time_ms));
    QString path = settings->value(S_SCENE,"").toString();   // sceneEd->text();
    outLog("Scanned ["+path+"] for threshold.xml files, on a thread.");
    outLog(msg);
    infoLabel5->setText(msg);
    setStatusMessage(msg);
    testAct6->setText("Re-&Load &Apt XML");   // &A
    test6_thread_done = true;
    // write cache file with list
    path = get_data_file(AIRPORT_CACHE);
    int res = loadair->writeAirportFile(path,pll);
    if (res) {
        msg.sprintf("%d",res);
        outLog("Write to "+path+" FAILED with error "+msg);
    } else {
        outLog("Written airport cache "+path);
    }
}

void MainWindow::on_test7_thread()
{
    bool ok = false;
    QString path = settings->value(S_FGAPTDAT,"").toString();
    QFile f(path);
    if (path.length() && f.exists()) {
        ok = true;
    } else {
        path = settings->value(S_ROOT,"").toString();   // sceneEd->text();
        if (util_isValidFGRootDir(path)) {
            path.append("/Airports/apt.dat.gz");
            QFile file(path);
            if (file.exists()) {
                ok = true;
                settings->setValue(S_FGAPTDAT,path);
                fg_apt_dat_file = path;
                infoLabel7->setText("FG Apt.dat: "+path);
                saveAptDatList(path);
            }
        }
    }
    // advise what is happening...
    QString msg("Loading ");
    msg.append(path);
    if (ok) {
        msg.append("... on a thread...");
        if (airdata) {
            airdata->clear_list(); // clear any previous list
        } else {
            airdata = new loadAptDat;
        }
        connect(airdata,SIGNAL(load_done()),this,SLOT(on_test7_done()));
        airdata->loadOnThread(path);
    } else {
        msg.append(" FAILED!");
    }
    infoLabel6->setText(msg);
    setStatusMessage(msg);
}

void MainWindow::on_test7_done()
{
    QString msg;
    int apcnt = airdata->getAirportCount();
    int runcnt = airdata->getRunwayCount();
    msg.sprintf("Apt.dat load: %d airports, %d runways, ",apcnt,runcnt);
    msg.append("in "+getElapTimeStg(airdata->getLoadTime()));
    outLog(msg);
    infoLabel6->setText(msg);
    setStatusMessage(msg);
#ifdef USE_ALLOC_DIALOG
    if (movedialog)
        movedialog->done_list = false;  // reset to re-populate list
#endif // #ifdef USE_ALLOC_DIALOG
    testAct7->setText("Re-&Load Apt.dat");   // &L
    test7_thread_done = true;
}

void MainWindow::on_test8()
{
    searchDialog d(this);
    d.exec();
}

void MainWindow::saveAptDatList(QString file)
{
    QFile f(file);
    if (file.length() && f.exists()) {
        if (!aptdat_list.contains(file)) {
            aptdat_list += file;
            settings->setValue(S_LISTFGAPTDAT,aptdat_list);
        }
    }
}


//     testAct9 = menuTest->addAction(tr("Set D&ata files..."),this,SLOT(543())); // &A
void MainWindow::on_test9()
{
    QString aptdat(fg_apt_dat_file);
    if (aptdat.length() == 0) {
        aptdat = settings->value(S_ROOT, "").toString();
        if (aptdat.length()) {
            aptdat.append("/Airports/apt.dat.gz");
        }
    }
    // QString title(tr("Get Existing apt.dat File"));
    getAptDat d(this);
    d.setup(aptdat,aptdat_list);
    d.exec();
    if (d.got_ok) {
        QString nfile(d.getFile());
        QFile f(nfile);
        if (nfile.length() && f.exists() && (nfile != fg_apt_dat_file)) {
            fg_apt_dat_file = nfile;                    // set NEw file
            infoLabel7->setText("FG Apt.dat: "+nfile);  // SHOW IT
            settings->setValue(S_FGAPTDAT,nfile);       // SAVE IT
            saveAptDatList(nfile);                      // and to LIST
        }
    }
}

void MainWindow::set_aircraft_path(QString path)
{
    QDir dir(path);
    if (dir.exists())
        settings->setValue(S_AIRPATH, path);
}

QString MainWindow::get_aircraft_path()
{
    QString tmp;
    QDir dir;
    tmp = settings->value(S_AIRPATH,"").toString();
    if (tmp.length() && dir.exists(tmp))
        return tmp;
    tmp = settings->value(S_ROOT, "").toString();
    if (!dir.exists(tmp)) {
        return "";
    }
    tmp.append("/Aircraft");
    if (!dir.exists(tmp)) {
        return "";
    }
    return tmp;
}

void MainWindow::on_test10()
{
    QString tmp, msg;
    QDir dir;
    tmp = get_aircraft_path();
    if ((tmp.length() == 0) || !dir.exists(tmp) ) {
        msg = "Directory of "+tmp+" does NOT exist! NO aircraft load!";
        setStatusMessage(msg);
        return;
    }
    if (!aeroLoad)
        aeroLoad = new loadAircraft(this);
    //aeroLoad->scanAircraft(tmp);
    aero_tm.start();
    msg = "Scanning directory "+tmp+" for *-set.xml files...on thread...";
    setStatusMessage(msg);
    connect(aeroLoad,SIGNAL(aero_load_done()),this,SLOT(on_test10_done()));
    paeroItem = aeroLoad->scanAircraft_on_thread(tmp);
}

void MainWindow::on_test10_done()
{
    // seem to be getting MULTIPLE call backs, so try
    disconnect(aeroLoad,SIGNAL(aero_load_done()),this,SLOT(on_test10_done()));
    QString dir, tmp, msg;
    dir = aeroLoad->get_Last_Dir(); // al.dir;
    msg = "Done "+dir+", ";

    int cnt = aeroLoad->getAeroListCount();
    tmp.sprintf("got %d aircraft, in ",cnt);
    msg.append(tmp);
    msg.append(getElapTimeStg(aero_tm.elapsed()));
#ifdef ADD_TAB_WIDGET
    if (aeroWidget) {
        aeroWidget->load_tree();
        msg.append(" - tree loaded");
    }
#endif
    setStatusMessage(msg);
    if (cnt)    // if ok, store aircaft directory path
        set_aircraft_path(dir);
    infoLabel8->setText("Load aircraft xml: "+dir);
    test10_thread_done = true;

    // output cache file
    tmp = get_data_file(AIRCRAFT_CACHE);
    aeroLoad->writeAeroFile(tmp, aeroLoad->getAeroListPtr());
    msg = "aircraft cache: ";
    msg.append(tmp);
    outLog(msg);
#ifdef ADD_TAB_WIDGET
    infoLabel9->setText(msg); // aircraft cache
#endif // ADD_TAB_WIDGET

}

void MainWindow::load_aircraft_cache()
{
    QString file1;
    file1 = get_data_file(AIRCRAFT_CACHE);
    QFile file(file1);
    if (!file.exists()) {
        outLog("File "+file1+" does NOT exist! No load done");
        return;
    }
    if (!aeroLoad)
        aeroLoad = new loadAircraft(this);

    PAEROLIST pal = new AEROLIST;
    aeroLoad->clear_list(pal);
    if ( aeroLoad->loadAeroFile(file1, pal) )
    {
#ifdef ADD_AIRPORT_WIDGET //* airports
        if (aeroWidget)
            aeroWidget->load_tree_from_list(pal);
#endif // #ifdef ADD_AIRPORT_WIDGET
    }
    aeroLoad->clear_list(pal);
    delete pal;
    outLog("Done File "+file1+" load");
}


void MainWindow::on_test11()
{
    QString file1;
    file1 = get_data_file(AIRCRAFT_CACHE);
    QFile file(file1);
    if (!file.exists()) {
        outLog("File "+file1+" does NOT exist! No load done");
        return;
    }
    if (!aeroLoad)
        aeroLoad = new loadAircraft(this);

    PAEROLIST pal = aeroLoad->getAeroListPtr();
    aeroLoad->clear_list(pal);
    if ( aeroLoad->loadAeroFile(file1, pal) ) {
        QString file2 = get_data_file("tempaero2.txt");
        aeroLoad->writeAeroFile(file2,pal);
    }
}

#ifdef ADD_TAB_WIDGET
void MainWindow::on_tab_changed(int tab_idx)
{
    QString msg;
    msg.sprintf("on_tab_changed: value %d, ",tab_idx);
    if (in_setup) {
        msg.append("still in setup");
    } else {
        settings->setValue(S_TABIDX,tab_idx);
        msg.append("saved to ");
        msg.append(S_TABIDX);
    }
    outLog(msg);
}
#endif

void MainWindow::on_test100()
{
    outLog("Log mark", (olflg_AddLE | olflg_AddTm));
}

// return a list of valid scenery directories
// QStringList XSettingsModel::getSceneryDirs()
QStringList MainWindow::getSceneryDirs()
{
    QStringList list;
    QString path;
//    QString msg;
    QDir dir;
//    QFile file;
    //int i, ind, j;
//#ifdef Q_OS_WIN
//    QChar psep(';');
//#else
//    QChar psep(':');
//#endif

    // 1 - get FG_ROOT/Scenery
    //path = fgroot("/Scenery");
    path = current_root;
    path.append("/Scenery");
    //ind = path.indexOf(QChar('"'));
    //if (ind == 0)
    //    path = path.mid(1,msg.length()-2);
    if ( !list.contains(path) ) {
        if (dir.exists(path)) {
            list += path;
        }
    }
    if (dir.exists(current_scenery)) {
        if ( !list.contains(current_scenery) ) {
            list += current_scenery;
        }

    }
    // 2 - if terrasync enabled
    //if (terrasync_enabled()) {
    //    path = terrasync_data_path();
    //    ind = path.indexOf(QChar('"'));
    //    if (ind == 0)
    //        path = path.mid(1,msg.length()-2);
    //    if ( !list.contains(path) ) {
    //        if (dir.exists(path)) {
    //            list += path;
    //        }
    //    }
    //}
    // 3 - if custom scenery enabled
    //if (custom_scenery_enabled()) {
    //    path = custom_scenery_path();
    //    ind = path.indexOf(QChar('"'));
    //    if (ind == 0)
    //        path = path.mid(1,msg.length()-2);
    //    if ( !list.contains(path) ) {
    //        if (dir.exists(path)) {
    //            list += path;
    //        }
    //   }
    //}

    // 4 - check fgfs command for any others, avoiding DUPLICATION
    //QStringList fgfs_args = get_fgfs_args();
    //for (i = 0; i < fgfs_args.size(); i++) {
    //    msg = fgfs_args.at(i);
    //    ind = msg.indexOf(QChar('"'));
    //    if (ind == 0)
    //        msg = msg.mid(1,msg.length()-2);
    //    if (msg.indexOf("--fg-scenery=") == 0) {
    //        msg = msg.mid(13);
    //        ind = msg.indexOf(QChar('"'));
    //        if (ind == 0)
    //            msg = msg.mid(1,msg.length()-2);
    //        QStringList path_list = msg.split(psep);
    //        for (j = 0; j < path_list.size(); j++) {
    //            path = path_list.at(j);
    //            ind = path.indexOf(QChar('"'));
    //            if (ind == 0)
    //                path = path.mid(1,msg.length()-2);
    //            if ( !list.contains(path) ) {
    //                if (dir.exists(path)) {
    //                    list += path;
    //                }
    //            }
    //        }
    //    }
    //}
    return list;
}



// eof - mainwindow.cpp
