#ifndef MOVEDIALOG_H
#define MOVEDIALOG_H

#include "app_config.h"
#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QToolButton>
#include <QGridLayout>
#include <QPushButton>
#include <QListWidget>
#include <QAbstractItemView>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QHeaderView>
#include <QProgressDialog>
#include <QSize>
#include "loadAptDat.h"

class MainWindow;

class moveDialog : public QDialog
{
    Q_OBJECT
public:
    explicit moveDialog(QWidget *parent = 0);
    MainWindow *main;
    int max_items;
    AIRPORTLIST *air_list;

    QLineEdit *latEd;   // = new QLineEdit(this);
    QLineEdit *lonEd;   // = new QLineEdit(this);
    QLineEdit *zoomEd;
    void finalise();
    void select_table_row(QString clat,QString clon,bool sel = true);
    bool done_list;

signals:
    void set_position(QString,QString,int);

public slots:
    void on_ok();
    void on_cancel();
#ifdef ADD_LIST_WIDGET
    void on_selection_changed();
#endif // #ifdef ADD_LIST_WIDGET
    void on_lat_changed(QString);
    void on_lon_changed(QString);
    void on_lat_finished();
    void on_lon_finished();

private:
    QGridLayout *mainLayout;    // = new QGridLayout(this);
    QLabel *infoLab;       // = new QLabel("Enter new lat/lon location",this);
    QLabel *latLab;        // = new QLabel("Lat:",this);
    QLabel *lonLab;        // = new QLabel("Lon:",this);
    QLabel *zoom1Lab;      // = new QLabel("Zoom:",this);
    QLabel *zoom2Lab;      // = new QLabel(msg,this);
    //QListWidget *listWidget;    // = new QListWidget(this);
    QTableWidget *listWidget;   // = new QTableWidget(this);
    QPushButton *okButton;  // = new QPushButton(this);
    QPushButton *cancelBut; // = new QPushButton(this);
    QLabel *selectLab;      // = new QLabel(this);

};

#endif // MOVEDIALOG_H
