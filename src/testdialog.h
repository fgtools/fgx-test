#ifndef TESTDIALOG_H
#define TESTDIALOG_H

#include "app_config.h"
#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QToolButton>
#include <QGridLayout>
#include <QPushButton>
#include <QCheckBox>

class MainWindow;

class testDialog : public QDialog
{
    Q_OBJECT
public:
    explicit testDialog(QWidget *parent = 0);
    MainWindow *main;

    QGridLayout *mainLayout;    // = new QGridLayout(this);
    QLabel *info;           // = new QLabel("Testing Native get Directory and File",this);
    QLabel *gdLab;          // = new QLabel("Directory:",this);
    QLineEdit *gdEd;        // = new QLineEdit(this);
    QPushButton *getDir;
    QLabel *gfLab;          // = new QLabel("File:",this);
    QLineEdit *gfEd;        // = new QLineEdit(this);
    QLabel *fiLab;          // = new QLabel("Filter:",this);
    QLineEdit *fiEd;        // = new QLineEdit(this);
    QLabel *opLab;          // = new QLabel("Options:",this);

    QLineEdit *optVal;      // = new QLineEdit(this);
    QCheckBox *sdo;         // = new QCheckBox("ShowDirsOnly",this); // if (options & 0x0001)
    QCheckBox *drsl;        // = new QCheckBox("DontResolveSymlinks",this); // if (options & 0x0002)
    QCheckBox *dcow;        // = new QCheckBox("DontConfirmOverwrite",this); // if (options & 0x0004)
    QCheckBox *dus;         // = new QCheckBox("DontUseSheet",this); // if (options & 0x0008)
    QCheckBox *dund;        // = new QCheckBox("DontUseNativeDialog",this); // if (options & 0x0010)
    QCheckBox *ro;          // = new QCheckBox("ReadOnly",this); // if (options & 0x0020)
    QCheckBox *hnfd;        // = new QCheckBox("HideNameFilterDetails",this); // if (options & 0x0040)

    QPushButton *getFile;   // = new QPushButton("Get File Name",this);
    QPushButton *getNFile;  // = new QPushButton("Get New File",this);

    QPushButton *ok;        // Done - close dialog

    int m_Options;
    void set_optVal();

signals:

public slots:
    void on_ok();
    void on_get_dir();
    void on_get_file();
    void on_get_nfile();
    void on_sdo(bool b);
    void on_drsl(bool b);
    void on_dcow(bool b);
    void on_dus(bool b);
    void on_dund(bool b);
    void on_ro(bool b);
    void on_hnfd(bool b);


};

#endif // TESTDIALOG_H
