#ifndef LOADAIRCRAFT_H
#define LOADAIRCRAFT_H
#include "app_config.h"
#include <QObject>
#include <QWidget>
#include <QString>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QDomDocument>
#include <QDomNodeList>
#include <QDomNode>
#include <QList>
#include <QStringList>
#include <QXmlQuery>
#include <QXmlResultItems>
#include <QXmlItem>
#include <QXmlNodeModelIndex>
#include <QTime>
#include "utilities/workThread.h"

class MainWindow;

#define omf_Append  0x0001

typedef struct tagAEROITEM {
    QString directory;
    QString aero;
    QString xml_file;
    QString description;
    QString fdm;
    QString author;
    QString file_path;
}AEROITEM, * PAEROITEM;

typedef QList<PAEROITEM> AEROLIST;
typedef AEROLIST * PAEROLIST;

#define af_Append   0x0001

typedef struct tagAEROLOAD {
    WORKITEM work;  // MUST be first in structure
    QString dir;    // directory to scan
    PAEROLIST pal;  // aero list to fill
    int flag;       // action flag
    bool in_load;   // TRUE when thread running
    int threadnum;   // returned by workThread
    // more???
}AEROLOAD, * PAEROLOAD;

class loadAircraft : public QObject
{
    Q_OBJECT
public:
    explicit loadAircraft(QObject *parent = 0);
    ~loadAircraft();
    MainWindow *main;

    PAEROLIST getAeroListPtr() { return &my_aeroList; }
#if 0
    void scanAircraft(QString path);
    void scanAircraft_test(QString path);
#endif // 0

    PAEROLOAD scanAircraft_on_thread(QString path, int flag = 0, PAEROLIST pal = 0 );

    void clear_list(PAEROLIST pal);
    void add2List(QString &directory, QString &aero, QString &xml_file, QString &description, QString &fdm,
                                QString &author, QString &file_path);
    int writeAeroFile(QString file, PAEROLIST pal, QString sep = "\t", int flag = 0);
    int loadAeroFile(QString file, PAEROLIST pal);
    int getAeroListCount() { return getAeroListPtr()->count(); }
    bool isThreadInFunction();

    QString current_path;
    PAEROITEM getFirstAero();
    PAEROITEM getNextAero();
    QString get_Last_Dir() { return al.dir; }

signals:
    void aero_load_done();

public slots:
    void aero_thread_done(int,int);

private:
    AEROLIST my_aeroList;
    AEROLOAD al;
    static void _loadStatic(void *vp);
    workThread *pworkThread;
    int next_aero;
};


#endif // LOADAIRCRAFT_H
