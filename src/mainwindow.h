/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Sep 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "app_config.h"
#include <QtGui/QMainWindow>
#include <QWidget>
#include <QString>
#include <QStringList>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QStatusBar>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSettings>
#include <QCloseEvent>
#include <QMessageBox>
#include <QTimer>
#include <QTime>    // elapsed time
#include <QDesktopServices>
#include <QGroupBox>
#include <QDir>
#include <QDirIterator>
#include <QDomDocument>
#include <QDomNode>
#include <QDomNodeList>
#ifdef ADD_TAB_WIDGET
#include <QTabWidget>
#include <QPlainTextEdit>
#include "aircraftWidget.h"
#ifdef ADD_AIRPORT_WIDGET
#include "airportsWidget.h"
#endif // #ifdef ADD_AIRPORT_WIDGET
#endif
#include "testdialog.h"
#include "moveDialog.h"
#include "loadAirports.h"
#include "loadAptDat.h"
#include "loadAircraft.h"

class osmMap;

enum TAB_PAGES {
    T_OSMMAP = 0,
    T_AERO,
#ifdef ADD_AIRPORT_WIDGET
    T_AIRPORTS,
#endif // ADD_AIRPORT_WIDGET
    T_RESULTS,
    T_MAX_VAL
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QWidget *widget;    // = new QWidget(this);
    QVBoxLayout *mainLayout;    // = new QVBoxLayout(this);
    QMenuBar *menuBar;          // = new QMenuBar(this);
    QSettings *settings;

    QMenu *menuFile;    // = new QMenu(tr("&File"),this);
    QAction *exitAct;   // = menuFile->addAction(tr("&Quit"),this,SLOT(on_exit()));

    QMenu *menuTest;    // = new QMenu(tr("&Test"),this);
    QAction *testAct;   // = menuTest->addAction(tr("T&est 1"),this,SLOT(on_test1()));
    QAction *testAct2;  // = menuTest->addAction(tr("T&est 2"),this,SLOT(on_test2()));
    QAction *testAct3;  // = menuTest->addAction(tr("&Get Dir..."),this,SLOT(on_test3()));
    QAction *testAct4;  // = menuTest->addAction(tr("&Native Get..."),this,SLOT(on_test3()));
    QAction *testAct5;  // = menuTest->addAction(tr("&Set Paths..."),this,SLOT(on_test5()));
    QAction *testAct6;  // = menuTest->addAction(tr("&Load Airports..."),this,SLOT(on_test6_thread()));
    QAction *testAct7;  // = menuTest->addAction(tr("&Load Apt.dat..."),this,SLOT(on_test7()));
    QAction *testAct8;  // = menuTest->addAction(tr("&Search ICAO..."),this,SLOT(on_test8()));
    QAction *testAct9;  // = menuTest->addAction(tr("Set D&ata files..."),this,SLOT(on_test9())); // &A
    QAction *testAct10; // load aircraft *-set.xml files
    QAction *testAct11; // load aircraft cache file

    QAction *testAct100;    // mark the LOG with a time

    QMenu *menuHelp;    // = new QMenu(tr("&Help",this));
    QAction *helpAct;   // = menuFile->addAction(tr("&About"),this,SLOT(on_about()));
    QAction *abtqtAct;   // = menuFile->addAction(tr("&About Qt"),this,SLOT(on_about_qt()));

    bool debug_mode;
    osmMap *osmmap;
#ifdef ADD_TAB_WIDGET
    QTabWidget *tabWidget;
    AircraftWidget * aeroWidget;
    QWidget *resultsWidget;
    QLabel *infoLabel8; // load aircraft xml
    QLabel *infoLabel9; // aircraft cache
    QPlainTextEdit *resultsEdit;
#ifdef ADD_AIRPORT_WIDGET
    //*airports
    AirportsWidget *airportWidget;
#endif // #ifdef ADD_AIRPORT_WIDGET
#endif

    QGroupBox *resultsGroup;    // = new QGroupBox(tr("Test Results"),this);
    QVBoxLayout *resultsLayout; // = new QVBoxLayout(this);
    QLabel *infoLabel;  // = new QLabel(settings->value(S_FILENAME, "NewProfile.ini").toString(),this);
    QLabel *infoLabel2;  // = new QLabel(settings->value(S_DIRNAME, util_getCurrentWorkDirectory()).toString();
    QLabel *infoLabel3;
    QLabel *infoLabel4;
    QLabel *infoLabel5;
    QLabel *infoLabel6; // = new QLabel("Load Apt.dat: ",this);
    QLabel *infoLabel7;


    QStatusBar *statusBar;  // = new QStatusBar(this);
    QLabel *labelTime;      // = new QLabel(tr("00:00:00"),this);
    QString last_status_msg;
    QTimer *timer;          // = new QTimer(this);

    QTime m_time;
    bool done_setmap;

    void closeEvent(QCloseEvent *event);
    QString get_data_file(QString);
    testDialog *testdialog;  // = new testDialog(this);
#ifdef USE_ALLOC_DIALOG
    moveDialog *movedialog;
#endif // #ifdef USE_ALLOC_DIALOG
    void setStatusMessage(QString msg, int timeout = DEF_TIMEOUT);
#ifdef LOADAIRPORTS_H
    loadAirports *loadair;
#endif // #ifdef LOADAIRPORTS_H
#ifdef LOADAPTDAT_H
    loadAptDat *airdata;
#endif // #ifdef LOADAPTDAT_H

    // experiment with keeping a LIST of valid paths
    QStringList root_list;
    QStringList scene_list;
    QStringList aptdat_list;
    void saveAptDatList(QString file);

    loadAircraft *aeroLoad;
    PAEROLOAD paeroItem;

    QTime aero_tm;
    bool auto_load_aptdat;
    bool auto_load_xml;
    bool auto_load_aircraft;
    void set_aircraft_path(QString path);
    QString get_aircraft_path();
    void load_aircraft_cache();
    void load_airport_cache();
    QStringList getSceneryDirs();

public slots:
    void on_exit();
    void on_about();
    void on_about_qt();
    void on_time_out();
    void on_test1();
    void on_test2();
    void on_test3();
    void on_test4();
    void on_test5();
    void on_test6_thread();
    void on_test6_done();
    void on_test7_thread();
    void on_test7_done();
    void on_test8();
    void on_test9();
    void on_test10();
    void on_test10_done();
    void on_test11();
    void on_test100();
    void move_map_position(QString lat, QString lon, int zoom);
    void on_message_changed(QString);
#ifdef ADD_TAB_WIDGET
    void on_tab_changed(int);
#endif

private:
    bool done_init;
    bool test7_thread_done;
    bool done_init6;
    bool test6_thread_done;
    bool test10_thread_done;
    bool in_setup;
    QString fg_apt_dat_file;
    QString current_root;   // if the root changes, re-load aircraft
    QString current_scenery;
    LOADLIST m_loadList;
};

#endif // MAINWINDOW_H
