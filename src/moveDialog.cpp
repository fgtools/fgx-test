/* ==================================================
   qt_osm_map project
   Created: Geoff R. McLane - Sep 2011
   License: GPL2 (or later)
   With special thanks to Yves for FGx, and its map widget
   ================================================== */

#include <math.h>
#include "loadAptDat.h"
#include "osm_map.h"        // for min/max slider range
#include "utilities/utilities.h"    // outLog()
#include "mainwindow.h"
#include "moveDialog.h"

#define D_CLOSETO   0.001   // when matching to an airport

typedef struct tagMAPTABLE {
    QString icao;
    QString name;
    QString lat;
    QString lon;
    int zoom;
}MAPTABLE, * PMAPTABLE;

#define MXT_ITEMS    19
MAPTABLE mapTable[MXT_ITEMS] = {
    { "YGIL", "Gilgandra", YGIL_LAT, YGIL_LON, YGIL_ZOOM },
    { "YTWN", "Tooraweenah", "-31.441689", "148.900009", 14 },
    { "YSDU", "Dubbo"      , "-32.2174865", "148.57727", 14 },
    { "YNRM", "Narromine"  , "-32.2158175", "148.2266455", 14 },
    { "YCBB", "Coonabarabran", "-31.3316275", "149.2673545", 10 },
    { "YWRN", "Warren"       , "-31.7354785", "147.8039035", 14 },
    { "YCNM", "Coonamble"    , "-30.9803715", "148.3759295", 14 },
    { "LFPZ", "Saint Cyr l Ecole", "48.8136535", "2.0665855", 14 },
    { "LFPN", "Toussus Le Noble", "48.751923", "2.106138", 14 },
    { "LFPG", "Paris Charles De Gaulle", "49.0097445", "2.5626195", 12 },
    { "LFPO", "Paris Orly", "48.72698", "2.370448", 12 },
    { "VHSK", "SEK KONG", "22.436592", "114.080399", 14 },
    { "VHHH", "Hong Kong Intl", "22.3089945", "113.91461", 14 },
    { "VHXX", "X CLOSED Kai Tak", "22.314607", "114.204051", 14 },
    { "VMMC", "Macau Intl", "22.148675", "113.591412", 14 },
    { "KSFO", "San Francisco Intl", KSFO_LAT, KSFO_LON, KSFO_ZOOM },
    { "KTEX", "Telluride Rgnl", "37.953758", "-107.908481", 14 },
    { "KDRO", "DURANGO LA PLATA CO", "37.1515170", "-107.7537680", 14 },
    { "PASI", "SITKA ROCKY GUTIERREZ", "57.0471280", "-135.3615980", 14 }
};

moveDialog::moveDialog(QWidget *parent) :
    QDialog(parent)
{
    // establish contact with main
    main = (MainWindow *)parent;
    max_items = 0;  // signal using BIG list
    air_list = 0;

    setWindowTitle("GET LATITUDE, LONGITUDE and ZOOM");
    setMinimumWidth(550);
    int m = 10;
    int row = 0;
    int cols = 4;
    QString msg;

    //QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(m,m,m,m);
    mainLayout->setSpacing(10);

    setLayout(mainLayout);

    infoLab = new QLabel("Enter new lat/lon location",this);
    mainLayout->addWidget(infoLab,row,0,1,cols);
    row++;

    latLab = new QLabel("Lat:",this);
    latEd = new QLineEdit(this);
    lonLab = new QLabel("Lon:",this);
    lonEd = new QLineEdit(this);

    mainLayout->addWidget(latLab,row,0,1,1);
    mainLayout->addWidget(latEd,row,1,1,1);
    mainLayout->addWidget(lonLab,row,2,1,1);
    mainLayout->addWidget(lonEd,row,3,1,1);
    row++;

    connect(latEd,SIGNAL(textChanged(QString)),this,SLOT(on_lat_changed(QString)));
    connect(lonEd,SIGNAL(textChanged(QString)),this,SLOT(on_lon_changed(QString)));
    connect(latEd,SIGNAL(editingFinished()),this,SLOT(on_lat_finished()));
    connect(lonEd,SIGNAL(editingFinished()),this,SLOT(on_lon_finished()));

    zoom1Lab = new QLabel("Zoom:",this);
    zoomEd = new QLineEdit(this);
    msg.sprintf("Range %d to %d",MIN_SRNG,MAX_SRNG);
    zoom2Lab = new QLabel(msg,this);
    mainLayout->addWidget(zoom1Lab,row,0,1,1);
    mainLayout->addWidget(zoomEd,row,1,1,1);
    mainLayout->addWidget(zoom2Lab,row,2,1,cols-2);
    row++;

#ifdef ADD_LIST_WIDGET
    selectLab = new QLabel(this);
    mainLayout->addWidget(selectLab,row,0,1,cols);
    row++;
    //listWidget = new QListWidget(this);
    //listWidget->setBaseSize(200,16*14);
    //listWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    // no, use a 'table'
    listWidget = new QTableWidget(this);
    listWidget->setColumnCount(4);
    QStringList header;
    header << "ICAO" << "Latitude" << "Longitude" << "Name";
    listWidget->setHorizontalHeaderLabels(header);
    listWidget->horizontalHeader()->setResizeMode(3,QHeaderView::Stretch);
    listWidget->verticalHeader()->hide();
    listWidget->setAlternatingRowColors(true); // hard to see
    listWidget->setShowGrid(false);
    listWidget->horizontalHeader()->setClickable(false);
    listWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    listWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

    mainLayout->addWidget(listWidget,row,0,24,cols);
    row += 24;

#endif // ADD_LIST_WIDGET

    row++;
    cancelBut = new QPushButton(tr("Cancel"),this);
    okButton = new QPushButton(tr("Ok"),this);

    mainLayout->addWidget(cancelBut,row,1,1,2);
    mainLayout->addWidget(okButton, row,3,1,2);

    connect(cancelBut, SIGNAL(clicked()), this, SLOT(on_cancel()));
    connect(okButton,  SIGNAL(clicked()), this, SLOT(on_ok()));

    finalise();
}

void moveDialog::finalise()
{
#ifdef ADD_LIST_WIDGET
    if (done_list)
        return;
    QString msg;
    QProgressDialog progress(this);
    bool got_list = false;
    //if (main && main->airdata && main->airdata->airlist.count()) {
    if (main && main->airdata && main->airdata->getAirListPtr() && main->airdata->getAirListPtr()->count()) {
        got_list = true;
        air_list = main->airdata->getAirListPtr();
        max_items = air_list->count();
        msg.sprintf("Populating the airport list with %d items",max_items);
        main->setStatusMessage(msg,DEF_TIMEOUT);
        QSize size(320,100);
        progress.resize(size);
        progress.setRange(0, max_items);
        progress.setWindowTitle("Populate Selection List");
        progress.setLabelText("Clearing previous list...");
        progress.show();
        progress.repaint();
    }

    listWidget->setUpdatesEnabled(false);
    // clear the current list
    while (listWidget->rowCount() != 0) {
        listWidget->removeRow(0);
    }
    // update list
    int i;
    int max = MXT_ITEMS;
    QTableWidgetItem *item;
    QString txt;
    if (got_list) {
        // build a FULL table
        //statusBarTree->showMessage("Reloading cache");
        progress.setLabelText("Getting and sorting list...");
        progress.repaint();

        sortByICAO(air_list);
        msg.sprintf("Populating the airport list with %d items",max_items);
        progress.setLabelText(msg);
        for (i = 0; i < max_items; i++) {
            PAD_AIRPORT pad = air_list->at(i);
            listWidget->insertRow(i);
            txt = pad->icao;
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,0,item);
            txt.sprintf("%f", pad->clat);
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,1,item);
            txt.sprintf("%f", pad->clon);
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,2,item);
            txt = pad->name;
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,3,item);
            progress.setValue(i);
            progress.repaint();
        }
    } else {
        for (i = 0; i < max; i++) {
            listWidget->insertRow(i);
            txt = mapTable[i].icao;
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,0,item);
            txt = mapTable[i].lat;
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,1,item);
            txt = mapTable[i].lon;
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,2,item);
            txt = mapTable[i].name;
            item = new QTableWidgetItem(txt,0);
            item->setFlags(Qt::ItemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
            listWidget->setItem(i,3,item);
        }
    }

    if (got_list) {
        progress.setLabelText("Connect and Enable list updates...");
        progress.repaint();
    }

    connect(listWidget,SIGNAL(itemSelectionChanged()),this,SLOT(on_selection_changed()));

    listWidget->resizeRowsToContents();

    listWidget->setUpdatesEnabled(true);

    if (got_list) {
        progress.hide();
    }
    done_list = true;
#endif // #ifdef ADD_LIST_WIDGET
}

void moveDialog::select_table_row(QString clat, QString clon, bool sel)
{
#ifdef ADD_LIST_WIDGET
    int i;
    QString msg;
    PAD_AIRPORT pad;
    double close_to = D_CLOSETO;
    double lat = clat.toDouble();
    double lon = clon.toDouble();
    double tlat, tlon;
    if (max_items && air_list) {
        for (i = 0; i < max_items; i++) {
            pad = air_list->at(i);
            tlat = pad->clat;
            tlon = pad->clon;
            if ((fabs(tlat - lat) < close_to)&&
                (fabs(tlon - lon) < close_to)) {
                break;
            }
        }
        if (i < max_items) {
            if (main && main->airdata) {
                msg = "Selected: ";
                msg.append(main->airdata->getAiportStg(pad));
                selectLab->setText(msg);
            }
            listWidget->selectRow(i);
            if (sel)
                listWidget->setFocus();
        } else {
            selectLab->setText("");
            listWidget->selectRow(-1);
        }
    } else {
        for (i = 0; i < MXT_ITEMS; i++) {
            tlat = mapTable[i].lat.toDouble();
            tlon = mapTable[i].lon.toDouble();
            if ((mapTable[i].lat == clat)&&
                (mapTable[i].lon == clon)) {
                // strings match - select this row - HOW?
                break;
            } else if ((fabs(tlat - lat) < close_to)&&
                       (fabs(tlon - lon) < close_to)) {
                break;
            }
        }
        if (i < MXT_ITEMS) {
            listWidget->selectRow(i);
            if (sel)
                listWidget->setFocus();
        }
    }
#else // !#ifdef ADD_LIST_WIDGET
    Q_UNUSED(clat);
    Q_UNUSED(clon);
    Q_UNUSED(sle);
#endif // #ifdef ADD_LIST_WIDGET y/n
}

void moveDialog::on_ok()
{
    QString lat = latEd->text().trimmed();
    QString lon = lonEd->text().trimmed();
    int zoom = zoomEd->text().trimmed().toInt();
    QString msg;
    msg.sprintf("%d",zoom);
    if (lat.length() && lon.length()) {
        double dlat = lat.toDouble();
        double dlon = lon.toDouble();
        if ((dlat >= -90.0)&&(dlat <= 90.0)&&(dlon >= -180.0)&&(dlon <= 180.0)) {
            outLog("on_ok: emit set_position: lat="+lat+", lon="+lon+", zoom="+msg);
            emit set_position(lat,lon,zoom);
        } else {
            msg.sprintf("on_ok: lat/lon failed (%f,%f)",dlat,dlon);
            outLog(msg);
        }
    } else {
        outLog("on_ok: pos failed "+lat+", "+lon);
    }
    close();
}

void moveDialog::on_cancel()
{
    outLog("moveDialog::on_cancel()");
    close();
}

#ifdef ADD_LIST_WIDGET
void moveDialog::on_selection_changed()
{
    int i;
    //= No Selection
    if(listWidget->selectionModel()->hasSelection() == false) {
        return;
    }
    QTableWidgetItem *item = listWidget->currentItem();
    QString txt = item->text();
    int ccol = listWidget->currentColumn();
    int crow = listWidget->currentRow();
    if ((max_items > 0)&&(air_list != 0)) {
        QString slat,slon,msg;
        PAD_AIRPORT pad = 0;
        i = max_items;
        if (crow < max_items) {
            pad = air_list->at(crow);
            slat.sprintf("%f", pad->clat);
            slon.sprintf("%f", pad->clon);
            if (ccol == 0) {
                if (pad->icao == txt)
                    i = crow;
            } else if (ccol == 1) {
                if (slat == txt)
                    i = crow;
            } else if (ccol == 2) {
                if (slon == txt)
                    i = crow;
            } else if (ccol == 3) {
                if (pad->name == txt)
                    i = crow;
            }
        }
        if (i < max_items) {
            // found selection
            latEd->setText(slat);
            lonEd->setText(slon);
            zoomEd->setText("14");
            if (main && main->airdata && pad) {
                msg = "Selected: ";
                msg.append(main->airdata->getAiportStg(pad));
                selectLab->setText(msg);
            }
        }
    } else {
        i = MXT_ITEMS;
        if (crow < MXT_ITEMS) {
            if (ccol == 0) {
                if (mapTable[crow].icao == txt)
                    i = crow;
            } else if (ccol == 1) {
                if (mapTable[crow].lat == txt)
                    i = crow;
            } else if (ccol == 2) {
                if (mapTable[crow].lon == txt)
                    i = crow;
            } else if (ccol == 3) {
                if (mapTable[crow].name == txt)
                    i = crow;
            }
        }
        if ( !(i < MXT_ITEMS) ) {
            // search for text
            for (i = 0; i < MXT_ITEMS; i++) {
                if ((mapTable[i].icao == txt)||
                    (mapTable[i].lat == txt)||
                    (mapTable[i].lon == txt)){
                    break;
                }
            }
        }
        if (i < MXT_ITEMS) {
            // found selection
            latEd->setText(mapTable[i].lat);
            lonEd->setText(mapTable[i].lon);
            zoomEd->setText(QString::number(mapTable[i].zoom));
        }
    }
}
#endif // #ifdef ADD_LIST_WIDGET

// hmmm, I thought these would SLOW-UP the text editing.
// but it seems the search of about 26,000 airports for a match is quite quick,
// but maybe on slower machines???!!!???
void moveDialog::on_lat_changed(QString clat)
{
    //QString lat = latEd->text().trimmed();
    QString clon = lonEd->text().trimmed();
    select_table_row(clat, clon, false);
    //latEd->setFocus();
}

void moveDialog::on_lon_changed(QString clon)
{
    QString clat = latEd->text().trimmed();
    //QString clon = lonEd->text().trimmed();
    select_table_row(clat, clon, false);
    //lonEd->setFocus();
}

void moveDialog::on_lat_finished()
{
    QString clat = latEd->text().trimmed();
    QString clon = lonEd->text().trimmed();
    select_table_row(clat, clon, false);
}

void moveDialog::on_lon_finished()
{
    QString clat = latEd->text().trimmed();
    QString clon = lonEd->text().trimmed();
    select_table_row(clat, clon, false);
}



// eof - moveDialog.cpp
